<?php

use Zend\Config\Reader\Json;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Metadata\Metadata;
use Zend\Db\Metadata\Object\ColumnObject;
use Zend\Db\Metadata\Object\ConstraintObject;
use Zend\Db\Metadata\Object\TableObject;
use Zend\Mvc\Application;

error_reporting(E_ALL & ~E_NOTICE & ~E_USER_NOTICE);

$_SERVER['REQUEST_URI'] = '/';

require 'init_autoloader.php';

$application = Application::init(require 'config/application.config.php');
$createDB = isset($argv[1]) && !empty($argv) ? false : true;
//echo "\n\n" . print_r($argv, 1) . "\n";

$zodeken2 = new Zodeken2($application, __DIR__);
$zodeken2->run($createDB);

class Zodeken2
{
    /**
     * @var string
     */
    const DEFAULT_DB_ADAPTER_KEY = 'Zend\Db\Adapter\Adapter';

    /**
     *
     * @var array
     */
    protected $configs = array();

    /**
     *
     * @var Adapter
     */
    protected $dbAdapter;

    /**
     *
     * @var string
     */
    protected $moduleName;
    
    protected $modules;
    
    protected $_tables;
    
    protected $_relFksTables;

    /**
     * Nao me orgulho mas eh o que temos para o momento
     */
    protected $_fks;

    /**
     *
     * @var string
     */
    protected $workingDir;

    /**
     *
     * @param Application $application
     */
    public function __construct(Application $application, $workingDir)
    {
        $this->workingDir = $workingDir;

        do {
            $dbAdapterServiceKey = $this->prompt(
                'Service key for db adapter [Zend\Db\Adapter\Adapter]: '
            );

            if ('' === $dbAdapterServiceKey) {
                $dbAdapterServiceKey = self::DEFAULT_DB_ADAPTER_KEY;
            }

            if (!$application->getServiceManager()->has($dbAdapterServiceKey)) {
                $isAdapterOk = false;
                echo "Service key $dbAdapterServiceKey does exist", PHP_EOL;
            } else {
                $isAdapterOk = true;
            }

        } while (!$isAdapterOk);

        $this->dbAdapter = $application->getServiceManager()->get(
            $dbAdapterServiceKey
        );

        if (!$this->dbAdapter) {
            throw new Exception("Database is not configured");
        }
    }

    public function run($createDB = true)
    {
        //$configFile = require_once($this->workingDir . '/generator-data.php');
        //echo "Configs = \n". print_r($configFile, 1) . "\n";
        $this->configs = require_once($this->workingDir . '/generator-data.php');
        //echo "Configs = \n". print_r(array_keys($this->configs), 1) . "\n";
        //exit;
        // read ini configs
        //if (is_readable($configFile)) {
        ///    $iniReader = new Json();
        //    $configs = $iniReader->fromFile($configFile);
        //} else {
        //    echo "\nNotice: $configFile does not exist\n";
        //}
        $this->modules = array_keys($this->configs);
        $this->mountTables();
        if($createDB === true){
            $this->generateTables();
        }
        
        echo "\n\nWARNING: please backup your existing code!!!\n\n";

        do {
            $moduleName = $this->prompt("Enter module name: ");
        } while ('' === $moduleName);

        $this->moduleName = $moduleName;
        
        // $tableList = isset($this->configs['tables'])
        //     && isset($configs['tables'][$moduleName])
        //     && '' !== $configs['tables'][$moduleName]
        //     ? preg_split('#\s*,\s*#', $configs['tables'][$moduleName])
        //     : null;

        // if (null === $tableList) {
        //     $shouldContinue = $this->prompt(
        //         "Table list of $moduleName module is not set, "
        //             + "continue with ALL tables in db? y/n [y]: "
        //     );

        //     if ('n' === strtolower($shouldContinue)) {
        //         echo 'Exiting...', PHP_EOL;
        //     }
        // }
        
        //echo "Configs = \n". print_r($configs, 1) . "\n";

        //echo "Please wait...\n";

        //echo "Table List = \n". print_r($tableList, 1) . "\n";
        
        $serviceFactoryMethods = array();

        foreach ($this->_tables as $table => $info) {

            $tableName = $table;
            echo $tableName, "\n";

            $this->generateModel($table, $info);
            $this->generateMapper($table, $info);
            $this->generateController($table, $info);
            $this->generateView($table, array("create", "index", "delete"), $info);

            $serviceFactoryMethods[] = $this->getMapperFactoryCode($table, $info);
        }

        $factoryCode = $this->getModelFactoryCode(
            implode('', $serviceFactoryMethods)
        );

        $this->writeFile(
            sprintf(
                '%s/module/%s/src/%s/Model/ModelFactory.php',
                $this->workingDir,
                $this->moduleName,
                $this->moduleName
            ),
            $factoryCode,
            false,
            true
        );
    }
    
    protected function _cleanTable($table)
    {
        $this->dbAdapter->getDriver()->getConnection()->beginTransaction();

        $resultDrop = $this->dbAdapter->getDriver()->getConnection()->execute('SET foreign_key_checks = 0');
        $resultDrop = $this->dbAdapter->getDriver()->getConnection()->execute('DROP TABLE IF EXISTS ' . strtolower($table));
        $resultDrop = $this->dbAdapter->getDriver()->getConnection()->execute('SET foreign_key_checks = 1');

        $this->dbAdapter->getDriver()->getConnection()->commit();
    }
    
    protected function generateTables()
    {
        if (empty($this->_tables) || count($this->_tables) <= 0) {
            echo "Invalid Tables";
            exit;
        }
        foreach($this->_tables as $table => $info) {
            $this->_cleanTable($table);
            $result = $this->dbAdapter->query($info["sql"]["table"], Adapter::QUERY_MODE_EXECUTE);
            if (!empty($info["sql"]["alter"]) && count($info["sql"]["alter"]) > 0){
                foreach($info["sql"]["alter"] as $alter) {
                    //echo "\n Alter: " . print_r($alter, 1) . "\n";
                    $result2 = $this->dbAdapter->query($alter, Adapter::QUERY_MODE_EXECUTE);
                }
            }
            //echo "Table: " . print_r($info["sql"], 1) . "\n";
        }
    }
    
    protected function mountTables()
    {
        if (!empty($this->configs) && count($this->configs) > 0) {
            //echo "\n\n" . print_r($configs, 1) . "\n\n";
            foreach($this->modules as $moduleName) {
                //echo "ModuleName = \n" . print_r($moduleName, 1) . "\n";
                foreach ($this->configs[$moduleName] as $table => $config) {
                    //echo "\n\t" . print_r($table, 1) . "\n";
                    //exit;
                    $this->_tables[$table]["fields"] = $config;
                    if(!isset($this->_tables[$table]["fields"]["id"])) {
                        $this->_tables[$table]["fields"]["id"] = array(
                            "type" => "int(11)",
                            "index" => "1",
                        );
                    }
                    $this->_tables[$table]["sql"] = $this->array2Sql($table, $this->configs[$moduleName][$table]);
                }
            }
            //echo "\n\t" . print_r($this->_tables, 1) . "\n";
            //exit;
        } else {
            echo "Invalid Json file"; 
            exit;
        }
    }
    
    protected function array2Sql($tableName, array $fields) 
    {
        $sql = "";
        $sql_fields = "";
        $sql_indexes = "";
        $sql_fks = "";
        if (isset($tableName) && !empty($tableName) && count($fields) > 0) {
            //echo "\nTable2  Name: \n" . print_r($tableName, 1) . "\n\n";
            //echo "\nFields:\n" . print_r($fields, 1) . "\n\n";
            //exit;
            $tableName = strtolower($tableName);
            $sql = "CREATE TABLE `".$tableName."` (\n";
            $sql_fields[] = '`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY';
            foreach($fields as $fieldName => $field) {
                //$fieldName = lcfirst($this->toCamelCase($fieldName));
                if ($fieldName != "id") {
                    //$fieldName = strtolower($field["name"]);
                    //print_r($field[$fieldName]["type"]);echo "\r\n";
                    //$fieldType = strtolower($field[$fieldName]["type"]);
                    $null = isset($field["null"]) && $field["null"] == false ? ' NOT NULL' : ' NULL';
                    $default = "";
                    if (isset($field["default"]) && $field["type"] != "text") {
                        if ($field["type"] == "timestamp") {
                            $default = ' DEFAULT '.$field["default"].'';
                        } else {
                            $default = ' DEFAULT "'.$field["default"].'"';
                        }
                    }
                    $sql_fields[] = '`' . $fieldName . '` ' . $field["type"] . $null . ' ' . $default;
                    if (isset($field["index"]) && $field["index"] == true) {
                        $sql_indexes[] = 'KEY `' . $fieldName . '` (`' . $fieldName . '`)';
                    }
                    if (isset($field["relationship"]) && !empty($field["relationship"]["table"]) 
                        && !empty($field["relationship"]["fk"])) {
                        $sql_fks[] = 'ALTER TABLE `' . $tableName . '` ADD CONSTRAINT `fk_' . $tableName . '_' . $fieldName . '` FOREIGN KEY (`' . $fieldName . '`) REFERENCES `' . $field["relationship"]["table"] . '` (`' . $field["relationship"]["fk"] . '`);';
                    }
                }
            }
            //echo "\n\n" . print_r($sql_fields, 1) . "\n\n";
            //exit;
            $sql .= implode(", \n", $sql_fields);
            if (is_array($sql_indexes)) {
                $sql .= ", \n" . implode(",", $sql_indexes);
            }
            $sql .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"; 
        }
        return array(
            "table" => $sql,
            "alter" => $sql_fks,
        );
    }
    
    protected function _extractMappers($fields)
    {
        $codeOfController = array("", "");
        foreach($fields as $name => $field) {
            if (isset($field["relationship"]) && isset($field["relationship"]["table"])) {
                $entity = ucfirst($this->toCamelCase($field["relationship"]["table"]));
                $codeOfController[0] .= "\$mapper{$entity} = ModelFactory::getInstance()->get{$entity}Mapper();\n";
                $codeOfController[1] .= "\"".$field["relationship"]["table"]."s\" => \$mapper{$entity}->get".$entity."ModelsAsArray(),\n";
            }
        }
        return $codeOfController;
    }
    
    protected function generateController($table, $info)
    {
        $mappers = $this->_extractMappers($info["fields"]);
        $controllerName = $this->toCamelCase($table);
        $controllerNameMin = strtolower($controllerName);
        $modelName = $controllerName . "Model";
        $code = <<<TABLE
<?php

namespace $this->moduleName\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\\$controllerName\\$modelName;

class {$controllerName}Controller extends AbstractActionController
{

    protected \$_{$controllerNameMin} = null;
    
    public function __construct()
    {
        \$this->_{$controllerNameMin} = ModelFactory::getInstance()->get{$controllerName}Mapper();
    }
    
    public function indexAction()
    {
        {$mappers[0]}
        return new ViewModel(array(
            "elements" => \$this->_{$controllerNameMin}->get{$controllerName}Models(),
            "entity" => "{$controllerNameMin}",
            $mappers[1]
        ));
    }
    
    public function createAction()
    {
        {$mappers[0]}
        \$request = \$this->getRequest();
        if (\$request->isPost()) {
            \$model = new $modelName();
            \$model->exchangeArray(\$request->getPost());
            \$this->_{$controllerNameMin}->save{$controllerName}Model(\$model);
            return \$this->redirect()->toRoute(strtolower('$this->moduleName'), array("controller" => "$controllerNameMin"));
        } else {
            \$id = (int) \$this->params()->fromRoute('id', 0);
            if (!is_null(\$id) && \$id > 0) {
                \$model = \$this->_{$controllerNameMin}->get{$controllerName}Model(\$id);
                if (!is_object(\$model)) {
                    throw new \\Exception("Not found Model");
                }
            } else {
                \$model = new {$controllerName}Model();
            }
        }
        return new ViewModel(array(
            "model" => \$model,
            {$mappers[1]}
        ));
    }
    
    public function removeAction()
    {
        \$id = (int) \$this->params()->fromRoute('id', 0);
        if (!is_null(\$id) && \$id > 0) {
            \$model = \$this->_{$controllerNameMin}->get{$controllerName}Model(\$id);
            if (is_object(\$model)) {
                \$this->_{$controllerNameMin}->delete{$controllerName}Model(\$model);
            } else throw new \Exception("{$controllerName} Not Found", 1);
        }
        return \$this->redirect()->toRoute(strtolower('$this->moduleName'), array("controller" => "{$controllerNameMin}"));
    }
}
TABLE;
        $filename = sprintf(
            '%s/module/%s/src/%s/Controller/%sController.php',
            $this->workingDir,
            $this->moduleName,
            $this->moduleName,
            $controllerName,
            $controllerName
        );
        
        $this->writeFile($filename, $code, false, true);
    }
    
    protected function generateView($table, array $actions, $info)
    {
        $controllerName = $this->toCamelCase($table);
        $controllerNameMin = strtolower($controllerName);
        //echo 'table '; print_r($table); echo "\r\n";
        //echo 'info '; print_r($info); echo "\r\n";
        //echo 'controller ' . $controllerName . ' --- ' . $controllerNameMin . "\r\n";
        
        foreach($actions as $action) {
            $code = $this->getViewFromAction($action, $table);
            
            $filename = sprintf(
                '%s/module/%s/view/%s/%s/%s.phtml',
                $this->workingDir,
                $this->moduleName,
                strtolower($this->moduleName),
                str_replace('_', '-', $table),
                strtolower($action)
            );
            
            $this->writeFile($filename, $code, false, true);
        }
    }
    
    protected function getViewFromAction($action, $table) 
    {
        switch($action) {
            case "index":
                $code = $this->getIndexView($table);
                break;
            case "create":
                $code = $this->getCreateView($table);
                break;
            default:
                $code = "--";
        }
        return $code;
    }
    
    protected function _mountSelectFK($field, $propertie)
    {
        $fkTable = '$' . $propertie["relationship"]["table"] . 'Rows';
        $fkName = '$' . $propertie["relationship"]["table"] . 'Rows[$modelArray["'.$field.'"]]["name"]';
        $modelArray = '$modelArray[\'$field\']';
        $select = <<<HTML
<select name="$field" id="$field" class="form-control c-select">
                        <option value="0" selected>Selecione</option>
                        <?php foreach($fkTable as \$table):?>
                            <option value="<?php echo \$table['id']; ?>" <?php if (\$modelArray['$field'] > 0 && \$modelArray['$field'] == \$table['id']) : ?>selected<?php endif; ?>>
                                <?php echo \$table['name']; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
HTML;
        return $select;
    }
    
    protected function _mountForm($table)
    {
        //echo "Fields <pre>" . print_r($this->_tables[$table]["fields"], 1) . "</pre>";
        //exit;
        $elements = array();
        $this->_fks = array();
        foreach($this->_tables[$table]["fields"] as $field => $propertie) {
            if (strtolower($field) != 'id') :
                $entity = lcfirst($this->toCamelCase($field));
                $label = ucfirst($field);
                switch(strtolower($propertie["type"])) {
                    case "text":
                        $type = "<textarea class=\"form-control\" rows=\"2\" id=\"$entity\" name=\"$entity\" placeholder=\"$entity\"><?php echo \$modelArray['$field']; ?></textarea>";
                        break;
                    case "int(11)":
                        if (isset($propertie["relationship"]) && isset($propertie["relationship"]["table"])) {
                            $this->_fks[] = '$' . $propertie["relationship"]["table"] . 'Rows = $this->' . $propertie["relationship"]["table"] . 's;';
                            $type = $this->_mountSelectFK($field, $propertie);
                            $this->_relFksTables[$table][$field] = $propertie["relationship"]["table"];
                            break;
                        } else {
                            $type = "<input type=\"text\" class=\"form-control\" value=\"<?php echo \$modelArray['$field']; ?>\" id=\"$entity\" name=\"$entity\" placeholder=\"$entity\">";
                        }
                    case "int(1)":
                        if ($entity == "status") {
                            $type = <<<HTML
    <select name="status" id="status" class="form-control c-select">
                            <option>Selecione um Status</option>
                            <option value="0" <?php if (\$this->model->getStatus() == 0) echo 'selected'; ?>>Inativo</option>
                            <option value="1" <?php if (\$this->model->getStatus() == 1) echo 'selected'; ?>>Ativo</option>
                        </select>
HTML;
                        } else {
                            $type = "<input type=\"text\" class=\"form-control\" value=\"<?php echo \$modelArray['$field']; ?>\" id=\"$entity\" name=\"$entity\" placeholder=\"$entity\">";
                        }
                        break;
                    default:
                        $type = "<input type=\"text\" class=\"form-control\" value=\"<?php echo \$modelArray['$field']; ?>\" id=\"$entity\" name=\"$entity\" placeholder=\"$entity\">";
                }
                //echo "Pro <pre>" . print_r($propertie, 1) . "</pre>";
                $hide = strtolower($field) == "id" || strtolower($field) == "datecreate" ? "hide" : "";
                $elements[] = <<<HTML
                <div class="form-group row $hide">
                    <label for="inputEmail3" class="col-sm-2 form-control-label">$label</label>
                    <div class="col-sm-10">
                        $type
                    </div>
                </div>
HTML;
            else :
                $type = "<?php if (!empty(\$modelArray['id'])) : ?><input type=\"hidden\" value=\"<?php echo \$modelArray['$field']; ?>\" name=\"id\"><?php endif; ?>";
                $elements[] = <<<HTML
                $type
HTML;
            endif;
        }
        return array(
            "fks" => $this->_fks,
            "code" => implode("\n", $elements)
        );
        //echo "Elements <pre>" . print_r($elements, 1) . "</pre>";
        //exit;
        //$html = "";
    }
    
    // protected function _mountViewFks($fks){
    //     foreach($fks as $fk){
            
    //     }
    // }
    
    protected function getCreateView($table) 
    {
        $controller = strtolower($table);
        $moduleName = strtolower($this->moduleName);
        $elements = $this->_mountForm($table);
        $set = implode("\n", $elements['fks']);
        $code = <<<VIEW
<?php
\$modelArray = \$this->model->toArray();
\$fields = array_keys(\$modelArray);
$set
?>

<div class="box">
    <div class="box-header">
        <h2><?php echo \$this->translate(ucfirst('$controller'), __NAMESPACE__); ?> > <?php echo \$this->translate('Add', __NAMESPACE__); ?></h2>
        <small></small>
    </div>
    <div class="box-divider m-a-0"></div>
    <div class="box-body">
        <form role="form" method="post">
            <!-- Dynamic Elements -->
            $elements[code]
            <!-- End Dynamic Elements -->
            <div class="form-group row m-t-md">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn white"><?php echo \$this->translate(!empty(\$modelArray[\$field]) ? 'Edit' : 'Add', __NAMESPACE__); ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<?php echo \$this->basePath('js/$moduleName/$controller/create.js'); ?>"></script>
VIEW;
        return $code;
    }
    
    protected function getIndexView($table) 
    {
        //echo  " REL \n" . print_r($this->_relFksTables, 1) . "\n";
        $controller = strtolower($table);
        $moduleName = strtolower($this->moduleName);
        $set = implode("\n", $this->_fks);
        $relations = isset($this->_relFksTables[$table]) ? 
            "\$relations = " . var_export($this->_relFksTables[$table], 1) . ";" : "";
        $code = <<<VIEW
<?php
$set

$relations
?>
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header"><?php echo \$this->translate(ucfirst(\$this->entity), __NAMESPACE__); ?></h4>
    </div>
    <div class="col-lg-12">
        <a href="<?php 
            echo \$this->url(strtolower('$this->moduleName'), 
                array('controller' => $controller, 'action' => 'create'));?>" class="btn btn-primary btn-sm active" role="button"><?php echo \$this->translate("Add", __NAMESPACE__); ?></a>
    </div>
</div>
<?php if(isset(\$this->elements) && count(\$this->elements) > 0): ?>
<?php \$headerSetted = false; ?>
<?php \$statusIndex = false; ?>
    <div class="table-responsive">
        <table class="table table-hover">
            <?php foreach(\$this->elements as \$element):?>
                <?php if (!\$headerSetted):  ?>
                <thead>
                    <?php \$headers = array_keys(\$element->toArray()); ?>
                    <tr>
                        <?php foreach(\$headers as \$key => \$header):?>
                            <th><?php echo \$this->translate(ucfirst(\$header), __NAMESPACE__); ?></th>
                        <?php endforeach; ?>
                            <th><?php echo \$this->translate("Actions", __NAMESPACE__); ?></th>
                    </tr>
                    <?php \$headerSetted = true; ?>
                </thead>
                <tbody>
                <?php endif; ?>
                <tr data-id="<?php echo \$element->getId(); ?>">
                    <?php foreach(\$element->toArray() as \$key =>  \$value):?>
                        <td>
                            <?php if(strtolower(\$key) == "status"): ?>
                                <?php if (\$value == 1): ?>
                                    <?php echo \$this->translate("Ativo", __NAMESPACE__); ?>
                                <?php else: ?>        
                                    <?php echo \$this->translate("Inativo", __NAMESPACE__); ?>
                                <?php endif; ?>        
                            <?php else: ?>
                                <?php if (isset(\$relations) && !empty(\$relations) && array_key_exists(\$key, \$relations)):?>
                                    <?php \$rel = "\$" . \$relations[\$key] . "Rows"; ?>
                                    <?php eval("\\\$label = \$rel;"); ?>
                                    <?php echo \$this->translate(\$label[\$value]["name"], __NAMESPACE__); ?>
                                <?php else: ?>
                                    <?php echo \$this->translate(\$value, __NAMESPACE__); ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                        <td>
                            <a href="<?php echo \$this->url(strtolower('$this->moduleName'), 
                                array('controller' => $controller, 'action' => 'create', "id" => \$element->getId())); ?>" class="edit"><?php echo \$this->translate("edit", __NAMESPACE__); ?></a>
                            <a href="<?php echo \$this->url(strtolower('$this->moduleName'), 
                                array('controller' => $controller, 'action' => 'remove', "id" => \$element->getId())); ?>" class="delete"><?php echo \$this->translate("delete", __NAMESPACE__); ?></a>
                        </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
<script type="text/javascript" src="<?php echo \$this->basePath('js/$moduleName/$controller/index.js'); ?>"></script>
VIEW;
        return $code;
    }

    /**
     * Get code of model factory class for each module
     *
     * @param string $factoriesCode
     * @return string
     */
    protected function getModelFactoryCode($factoriesCode)
    {
        return

        $factoryCode = <<<MODULE
<?php

namespace $this->moduleName\Model;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ModelFactory implements ServiceLocatorAwareInterface
{

    /**
     * @var ServiceLocatorInterface
     */
    protected \$serviceLocator;

    /**
     * @var \\$this->moduleName\Model\ModelFactory
     */
    static protected \$instance;

    /**
     * @return \\$this->moduleName\Model\ModelFactory
     */
    static public function getInstance()
    {
        if (!self::\$instance) {
            self::\$instance = new self;
        }
        return self::\$instance;
    }

    private function __construct() {}

    private function __clone() {}
$factoriesCode

    public function setServiceLocator(ServiceLocatorInterface \$serviceLocator)
    {
        \$this->serviceLocator = \$serviceLocator;
    }

    public function getServiceLocator()
    {
        return \$this->serviceLocator;
    }
}
MODULE;
    }

    /**
     *
     * @param TableObject $table
     * @return string
     */
    protected function getMapperFactoryCode($table)
    {
        $tableName = strtolower($table);

        $modelName = $this->toCamelCase($tableName);

        $getMapperMethod = 'get' . $modelName . 'Mapper';
        $getTableGatewayMethod = 'get' . $modelName . 'TableGateway';

        return <<<CODE

    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function $getTableGatewayMethod()
    {
        \$dbAdapter = \$this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        \$resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        \$resultSetPrototype->setArrayObjectPrototype(new \\$this->moduleName\Model\\$modelName\\{$modelName}Model());
        return new \Zend\Db\TableGateway\TableGateway('$tableName', \$dbAdapter, null, \$resultSetPrototype);
    }

    /**
     * @return \\$this->moduleName\Model\\$modelName\\{$modelName}Mapper
     */
    public function $getMapperMethod()
    {
        \$tableGateway = \$this->$getTableGatewayMethod();
        \$mapper = new \\$this->moduleName\Model\\$modelName\\{$modelName}Mapper(\$tableGateway);
        \$mapper->setServiceLocator(\$this->serviceLocator);
        return \$mapper;
    }
CODE;
    }

    /**
     *
     * @param TableObject $table
     */
    protected function generateMapper($table, $info)
    {
        $modelName = $this->toCamelCase($table);

        $primaryKey = array("id");
        $indexes = array();
        $mappingCode = '';
        $indexCode = '';

        foreach ($info["fields"] as $field => $details)
        {
            /* @var $constraint ConstraintObject */
            //echo "\nField = \n" . print_r($field, 1) . "\n\n";
            //echo "\n Details \n" . print_r($details, 1) . "\n\n";
            if ((isset($details["index"]) && $details["index"] == true) || 
                (isset($details["relationship"]) && !empty($details["relationship"])) ) {
                $indexes[][] = $field;
            }
        }
        //echo "\n Aqui 1\n" . print_r($indexes, 1) . "\n\n";
        //exit;
        if (isset($indexes[0])) {
            $indexCodeArray = array();

            foreach ($indexes as $index)
            {
                $singleIndexCode = $this->getMethodsOfIndex($index, $modelName);

                if (!is_string($singleIndexCode)) {
                    continue;
                }

                $indexCodeArray[] = $singleIndexCode;
            }

            $indexCode = implode('', $indexCodeArray);
        }

        if (count($primaryKey) === 1) {
            $mappingCode = $this->getPrimaryKeyCode($primaryKey, $modelName);
        }

        $code = <<<TABLE
<?php

namespace $this->moduleName\Model\\$modelName;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

use Manager\Model\ModelFactory;

class {$modelName}Mapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected \$tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected \$serviceLocator;

    public function __construct(TableGateway \$tableGateway)
    {
        \$this->tableGateway = \$tableGateway;
    }

    public function get{$modelName}ModelByCriteria(array \$criteria)
    {
        return \$this->tableGateway->select(\$criteria);
    }

$mappingCode
$indexCode

    public function get{$modelName}ModelsAsArray() {
        \$arrayResult = array();
        \$rowset = \$this->get{$modelName}Models();
        foreach(\$rowset as \$row) {
            \$arrayResult[\$row->getId()] = \$row->toArray();
        }
        return \$arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface \$serviceLocator)
    {
        \$this->serviceLocator = \$serviceLocator;
    }

    public function getServiceLocator()
    {
        return \$this->serviceLocator;
    }
}
TABLE;
        $filename = sprintf(
            '%s/module/%s/src/%s/Model/%s/%sMapper.php',
            $this->workingDir,
            $this->moduleName,
            $this->moduleName,
            $modelName,
            $modelName
        );

        if (file_exists($filename)) {
            $existingCode = file_get_contents($filename);
            $startPoint = 'protected $serviceLocator;';
            $endPoint = 'public function __construct(TableGateway $tableGateway)';
            $customCode = substr($existingCode, strpos($existingCode, $startPoint) + strlen($startPoint), strpos($existingCode, $endPoint) - strpos($existingCode, $startPoint) - strlen($startPoint));

            $code = preg_replace('#' . preg_quote($startPoint, '#') . '\s*' . preg_quote($endPoint, '#') . '#si', "$startPoint$customCode$endPoint", $code);

            $startPoint = "namespace $this->moduleName\Model\\$modelName;";
            $endPoint = "class {$modelName}Mapper implements ServiceLocatorAwareInterface";

            $customCode = substr($existingCode, strpos($existingCode, $startPoint) + strlen($startPoint), strpos($existingCode, $endPoint) - strpos($existingCode, $startPoint) - strlen($startPoint));

            $code = preg_replace('#' . preg_quote($startPoint, '#') . '.*' . preg_quote($endPoint, '#') . '#si', "$startPoint$customCode$endPoint", $code);
        }

        $this->writeFile($filename, $code, false, true);
    }

    /**
     *
     * @param array $primaryKey
     * @param string $modelName
     * @return string
     */
    protected function getPrimaryKeyCode($primaryKey, $modelName)
    {
        $primaryKeyCamelCase = $this->toCamelCase($primaryKey[0]);

        return <<<CODE

    /**
     *
     * @return resultset
     */
    public function get{$modelName}Models()
    {
        \$tableGateway = \$this->tableGateway;
        \$rowset = \$tableGateway->select(function (Select \$select) {
             \$select->order('id DESC');
        });
        return \$rowset;
    }

    /**
     * @param int \$id
     * @return {$modelName}Model
     */
    public function get{$modelName}Model(\$id)
    {
        return \$this->tableGateway->select(array('$primaryKey[0]' => \$id))->current();
    }

    /**
     * @param {$modelName}Model \$model
     */
    public function save{$modelName}Model({$modelName}Model &\$model)
    {
        \$id = \$model->get$primaryKeyCamelCase();

        if (!\$id) {
            \$this->tableGateway->insert(\$model->toArray());
            \$model->setId(\$this->tableGateway->lastInsertValue);
            return \$model->getId();
        } else {
            \$this->tableGateway->update(\$model->toArray(), array('$primaryKey[0]' => \$id));
        }
    }

    /**
     *
     * @param {$modelName}Model|int \$model
     */
    public function delete{$modelName}Model(\$model)
    {
        if (\$model instanceof {$modelName}Model) {
            \$id = \$model->get$primaryKeyCamelCase();
        } else {
            \$id = \$model;
        }

        \$this->tableGateway->delete(array('$primaryKey[0]' => \$id));
    }
CODE;
    }

    /**
     *
     * @param type $index
     * @param string $modelName
     * @return string|boolean
     */
    protected function getMethodsOfIndex($index, $modelName)
    {
        //echo "\nAqui 2 \n" . print_r($index, 1) . "\n";
        $camelCaseColumns = $index;
        $functionNames = array();

        foreach ($camelCaseColumns as &$camelCaseColumn)
        {
            $camelCaseColumn = $this->toCamelCase($camelCaseColumn);
        }

        $vars = array();

        foreach ($camelCaseColumns as $var)
        {
            $var[0] = strtolower($var[0]);
            $vars[] = $var;
        }

        $functionNameResultSet = "get{$modelName}ModelSetBy" . implode('And', $camelCaseColumns);
        $functionNameResult = "get{$modelName}ModelsBy" . implode('And', $camelCaseColumns);

        if (isset($functionNames[$functionNameResult]) || isset($functionNames[$functionNameResultSet])) {
            return false;
        }

        $functionNames[$functionNameResult] = 1;
        $functionNames[$functionNameResultSet] = 1;

        $argListArray = array();
        $varCommentsArray = array();

        foreach ($vars as $var)
        {
            $argListArray[] = '$' . $var;
            $varCommentsArray[] = "     * @param mixed $$var";
        }
        $argList = implode(', ', $argListArray);
        $varComments = implode("\n", $varCommentsArray);

        $whereArray = array();

        foreach ($index as $offset => $indexColumn)
        {
            $whereArray[] = "'$indexColumn' => $$vars[$offset]";
        }

        $where = implode(",\n            ", $whereArray);

        return <<<CODE


    /**
     *
$varComments
     * @return {$modelName}Model
     */
    public function $functionNameResult($argList)
    {
        return \$this->tableGateway->select(array($where));
    }


    /**
     *
$varComments
     * @return ResultSet
     */
    public function $functionNameResultSet($argList)
    {
        return \$this->tableGateway->select(array($where));
    }
CODE;
    }

    protected function generateModel($table, $info)
    {
        $modelName = $this->toCamelCase($table);
        //echo "Table = " . $modelName . "\n";
        
        $fieldsCode = array();
        $getterSetters = array();

        foreach ($info["fields"] as $fieldName => $detail)
        {
            /* @var $column ColumnObject */
            //$fieldName = $column->getName();
            $fieldNameCamelCase = $varName = $this->toCamelCase($fieldName);
            $varName[0] = strtolower($varName[0]);
            //echo "\t\tfieldNameCamelCase = " . $modelName . " - " . $varName[0] .  "\n";
            //exit;
            //echo "--->> " . strtolower($fieldName) . "\n";
            
            if ($fieldName == 'id') :
                $defaultValue = "NULL";
            else :
                $defaultValue = "'".$detail["default"]."'";
            endif;
            
            
            $getterSetters[] = "
    public function get$fieldNameCamelCase()
    {
        return \$this->data['$fieldName'];
    }

    public function set$fieldNameCamelCase(\$$varName)
    {
        \$this->data['$fieldName'] = \$$varName;
    }";

            $fieldsCode[] = "
        '$fieldName' => $defaultValue,";
        }

        $fieldsCode = "array(" . implode('', $fieldsCode) . '
    )';
        $getterSettersCode = implode('', $getterSetters);

        $code = <<<MODEL
<?php

namespace $this->moduleName\Model\\$modelName;

class {$modelName}Model
{

    protected \$data = $fieldsCode;
$getterSettersCode

    public function exchangeArray(\$data)
    {
        foreach (\$data as \$key => \$value)
        {
            if (!array_key_exists(\$key, \$this->data)) {
                continue;//throw new \Exception("\$key field does not exist in " . __CLASS__);
            }
            if (\$key == "dateCreate") {
                \$this->data[\$key] = date("Y-m-d H:i:s");
            } else {
                \$this->data[\$key] = \$value;
            }
        }
    }

    public function toArray()
    {
        return \$this->data;
    }
    
    public function getArrayCopy()
    {
        return \$this->data;
    }
    
    public function getMyName()
    {
        return strtolower("{$modelName}");
    }
}
MODEL;

        $this->writeFile(
            sprintf(
                '%s/module/%s/src/%s/Model/%s/%sModel.php',
                $this->workingDir,
                $this->moduleName,
                $this->moduleName,
                $modelName,
                $modelName
            ),
            $code,
            false,
            true
        );
    }

    /**
     *
     * @return TableObject[]
     */
    protected function getTables()
    {
        $metadata = new Metadata($this->dbAdapter);

        return $metadata->getTables();
    }

    /**
     *
     * @param string $filename
     * @param string $contents
     * @param boolean $generatePatchIfExists
     * @param boolean $overwrite
     */
    protected function writeFile($filename, $contents, $generatePatchIfExists = true, $overwrite = false)
    {
        $dir = dirname($filename);
        //print_r($filename);echo "\r\n";

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        if ($overwrite) {
            file_put_contents($filename, $contents);
        } elseif ("service.config.zk" !== substr($filename, -17) && file_exists($filename)) {
            $zodekenFile = $filename . '.zk';
            file_put_contents($zodekenFile, $contents);

            if ($generatePatchIfExists && 'Linux' === PHP_OS) {
                `diff -u $filename $zodekenFile > $filename.patch`;
            }
        } else {
            file_put_contents($filename, $contents);
        }
    }

    /**
     *
     * @param string $name
     * @return string
     */
    protected function toCamelCase($name)
    {
        return implode('', array_map('ucfirst', explode('_', $name)));
    }

    /**
     *
     * @param string $message
     * @return string
     */
    protected function prompt($message)
    {
        echo $message;
        return trim(fgets(STDIN));
    }

}

