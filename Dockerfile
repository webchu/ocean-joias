FROM php:5.6-apache

RUN apachectl stop
RUN apt-get update && apt-get install -y \
  git zlib1g-dev libicu-dev nano curl vim zip \
  libfreetype6-dev \
    libmcrypt-dev \
    libpng12-dev \
    libjpeg-dev \
    libpng-dev \
  && docker-php-ext-install iconv mcrypt \
    && docker-php-ext-configure gd \
        --enable-gd-native-ttf \
        --with-freetype-dir=/usr/include/freetype2 \
        --with-png-dir=/usr/include \
        --with-jpeg-dir=/usr/include \
    && docker-php-ext-install gd \
    && docker-php-ext-install mbstring \
    && docker-php-ext-enable opcache gd \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
  && rm -r /var/lib/apt/lists/* \
  && curl -sS https://getcomposer.org/installer \
    | php -- --install-dir=/usr/local/bin --filename=composer \
  && a2enmod rewrite \
  && echo "AllowEncodedSlashes On" >> /etc/apache2/apache2.conf \
  && echo "ServerName localhost" >> /etc/apache2/apache2.conf \
  && rm -Rf /var/www/html

COPY ./config/php.ini /usr/local/etc/php/php.ini

COPY ./hdmotos/ /var/www/.

WORKDIR /var/www

RUN rm -Rf /var/www/html && cd /var/www && ln -s public html

RUN apachectl start

#RUN php /var/www/public/index.php development enable
RUN cd /var && chown -R www-data.www-data www/ && echo "alias ll=\"ls -lah\"" >> ~/.bashrc
RUN /usr/local/bin/composer install
EXPOSE 80