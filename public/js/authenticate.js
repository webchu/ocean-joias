$(function() {
    $('#formAuthenticate').validate({
        errorElement: 'span',
        rules: {
            email: {
                required: true,
                email: true
            },
            password: 'required',
            //pai: 'required',
        },
        messages: {
            email: 'Informe um e-mail válido',
            password: 'Informe a sua senha',
        },
        submitHandler: function(form) {
            var data = _.object(_.map($(form).serializeArray(), _.values));
            $.ajax({
                url: '/login',
                type: 'post',
                contentType : 'application/json',
                data: JSON.stringify(data),
            }).done(function(response) {
                if (response.status) {
                    alert('Usuário autenticado com suceso!\nAguarde enquanto você é redirecionado...');
                    var url = app.getParameterByName('u');
                    if (url !== '')
                        document.location.href = url;
                    else
                        document.location.href = '/painel/anuncios';
                }
            });
        }
    });
});