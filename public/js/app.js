//var app = {};
;(function ($, window, document, undefined) {
	app = {
		modal: {
			open: function(element, options) {
				options = options || {};
				$.subscribe('/modal/loaded', function() {
					var h = $('.modal-overlay .content').outerHeight(),
						w = $('.modal-overlay .content').outerWidth();
					$('.modal-overlay .content').css({
						'margin-top' : ((h/2)*-1) + 'px',
						'margin-left': ((w/2)*-1) + 'px'
					});
				});

				$('body').addClass('overlay-show');

				if (!$('body .modal-overlay').length) {
					$('body').append('<div class="modal-overlay"><div class="content"></div></div>');
				} else {
					$('.modal-overlay').show();	
				}

				var h = $('body').outerHeight();
				$('.modal-overlay').height(h).addClass('opened');
				$('.modal-overlay .content').append($(element).clone());

				$('a.close-modal', '.modal-overlay .content').on('click', function() {
					app.modal.close();
				});

				$.publish('/modal/loaded', {});
				
				if (typeof(options.onComplete) == 'function') {
					options.onComplete();
				}
			},
			close: function() {
				$('body').removeClass('overlay-show');
				$('.modal-overlay').fadeOut('fast', function() {
					$('.modal-overlay .content').empty();
					$('.modal-overlay').removeClass('opened');
				});
			}
		},
		popup: function(url, target, width, height){
			var left = screen.width/2 - width/2,
				top  = screen.height/2 - height/2;
			return window.open(url, target, "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width="+ width +", height="+ height +", top="+ top +", left="+ left);
		},
		getURLParameter: function(name, url) {
			return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || [, ""])[1].replace(/\+/g, '%20')) || null
		},
		getParameterByName: function(name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
				results = regex.exec(location.search);
			return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		},	
		/*getUrlParameter: function getUrlParameter(sParam) {
		    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		        sURLVariables = sPageURL.split('&'),
		        sParameterName,
		        i;

		    for (i = 0; i < sURLVariables.length; i++) {
		        sParameterName = sURLVariables[i].split('=');

		        if (sParameterName[0] === sParam) {
		            return sParameterName[1] === undefined ? true : sParameterName[1];
		        }
		    }
		},*/
		validate: {
			email: function validateEmail(email) {
			    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			    return re.test(email);
			}
		},
		currency: function(c, d, t) {
			var n = this, 
			    c = isNaN(c = Math.abs(c)) ? 2 : c, 
			    d = d == undefined ? "." : d, 
			    t = t == undefined ? "," : t, 
			    s = n < 0 ? "-" : "", 
			    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		},
	};

	$(function() {
		//app.modal();
	})
})(jQuery, window, document);

Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

String.prototype.formatDate = function() {
	var d = this.split('-');
	return [d[2], d[1], d[0]].join('/');
};