var mod = angular.module('partner_mod', []);

mod.directive('partner', function() {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: './views/partner.html',
		controller: function($scope, $window) {
			var _this = this;

			var categories = ['Restaurante', 'Cinema', 'Mercado'];
			$scope.list = PARTNERS;

			/*
			$scope.list = [];
			for (var x = 0; x < 50; x++) {
				$scope.list.push({
					id: x,
					name: 'Dado Bier',
					description: 'Lorem ipsum Laboris id do mollit veniam adipisicing commodo in non consectetur eiusmod dolore elit do aute ullamco esse.',
					url: 'http://www.dadobier.com.br',
					category: categories[Math.floor(Math.random() * categories.length)],
					image: 'dado_bier.png'
				});
			}*/

		    $scope.close = function() {
		    	$scope.selectedPartner = null;
		    }

		    $scope.selectPartner = function( partner ) {
		        $scope.selectedPartner = partner;
		    };
		}
	}
});