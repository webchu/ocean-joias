$(function() {
	$('[name="phone"], input[name="cellphone"]').mask('(99) 99999-9999');
    $('[name="postalcode"]').mask('99999-999');
    $('[name="document"]').mask('999.999.999-99');

    $('#formRegister').validate({
        errorElement: 'span',
        rules: {
            name: 'required',
            lastName: 'required',
            email: {
                required: true,
                email: true
            },
            phone: 'required',
            cellphone: 'required',
            document: 'required',
            address: 'required',
            city: 'required',
            state: 'required',
            password: 'required',
            password2: {
                equalTo: "#password"
            }
            //pai: 'required',
        },
        messages: {
            name: 'Informe o seu nome',
            email: 'Informe um e-mail válido',
            phone: 'Informe o seu telefone',
            cellphone: 'Informe o seu celular',
            password2: 'Repita a senha',
        },
        submitHandler: function(form) {
            var data = _.object(_.map($(form).serializeArray(), _.values));
            $.ajax({
                url: '/user',
                type: 'post',
                contentType : 'application/json',
                data: JSON.stringify(data),
            }).done(function(response, textStatus, xhr) {
                if (textStatus == 'success') {
                    alert('Usuário cadastrado com suceso!\nAguarde enquanto você é redirecionado para o Painel do Cliente...');
                    $('#formRegister')[0].reset();
                    var dataLogin = {
                        email: data.email,
                        password: data.password
                    };
                    $.ajax({
                        url: '/login',
                        type: 'post',
                        contentType : 'application/json',
                        data: JSON.stringify(dataLogin),
                    }).done(function(response) {
                        if (response.status) {
                            //alert('Usuário autenticado com suceso!');
                            document.location.href = '/painel';
                        }
                    });
                } else {

                }
            });
        }
    });
});