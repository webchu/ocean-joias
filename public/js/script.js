/*
---------------------------------
RETIRAR O SETTIMEOUT DE TUDO
PRECISEI COLOCAR POR CAUSA DO
CARREGAMENTO OS TEMPLATES
SEPARADOS
---------------------------------
*/

$(document).ready(function(){
	var width = $('body').outerWidth();
	if (width <= 550){
		slide('.family ul',2,2);
		slide('.showcase ul',2,2);
		$('body').addClass('size-xs');
		$('#filters').addClass('collapse');	
	} else if(width <= 991 ){
		slide('.family ul',3,3);
		slide('.showcase ul',3,3);
		$('body').addClass('size-sm');
		$('#filters').addClass('collapse');
	} else{
		slide('.showcase ul',4,4);
	}
    

    $('select[multiple]:not(.no-apply)').each(function(){
    	$(this).multiselect({ 
	    	texts: { 
	    		placeholder: $(this).attr('title'),
	    		selectedOptions:' selecionados',
	    		selectAll: 'Selecionar todos'
	    		},
	    	selectAll:true,
	    	minHeight:150,
	    	maxHeight:150
    	});
    });
    $('.itens').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  asNavFor: '.nav-itens'
	});
	$('.nav-itens').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  asNavFor: '.itens',
	  dots: false,
	  arrows: true,
	  centerMode: false,
	  focusOnSelect: true
	});
});

function slide(element,slidesToShow,slidesToScroll){
	$(element).slick({
		infinite: false,
		slidesToShow: slidesToShow,
		slidesToScroll:slidesToScroll,
		arrows:true,
		dots:false,
		slide: 'li',
		swipe:false
	});
};

jQuery.extend(jQuery.validator.messages, {
    required: "Este campo é obrigatório!",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Repita a informação",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});