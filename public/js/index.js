$(function() {
	$("#familia").change(function () {   
        var id = $(this).val();
        $('#modelo option:not(:first)').hide();
        $('#modelo').val(0).trigger('change');
        $('#modelo option[data-family="' + id + '"]').show();
    });	
});