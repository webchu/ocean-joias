$(function() {
	$('.table').DataTable( {
    	'ajax': {
    		url: '/favorite',
    		dataSrc: ''
    	},
    	'columns': [
    		{ 'data': 'id' },
    		{ 'data': 'model' },
    		{ 'data': 'name' },
    		{ 'data': 'price' }
    	],
    	'rowCallback': function(row, data) {
    		console.log(row, data);
    	},
    	'initComplete': function() {
    		console.log('finish');
    	}
	});
});