$(function() {
	$('input[name="update-password"]').on('change', function() {
		console.log($(this).is(':checked'));
		if ($(this).is(':checked')) {
			$('.row-update-password').show();
			$('input', '.row-update-password').removeAttr('disabled');
		} else {
			$('.row-update-password').hide();
			$('input', '.row-update-password').attr('disabled', true);
		}
	});
});