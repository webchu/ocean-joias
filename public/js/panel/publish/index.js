$(function() {
	$('.actions a').on('click', function() {
        var item = $(this).closest('li'),
            product_id = item.data('product-id'),
            action = $(this).data('action'),
            state = 0;

        switch (action) {
            case 'play': state = 1; break;
            case 'pause': state = 2; break;
            case 'delete': state = 3; break;
        }

        var data = {
            'state': state
        };
        
        $.ajax({
            url : '/api/product/' + product_id,
            data : JSON.stringify(data),
            type : 'PATCH',
            contentType : 'application/json',
            xhr: function() {
                return window.XMLHttpRequest == null || new window.XMLHttpRequest().addEventListener == null 
                    ? new window.ActiveXObject("Microsoft.XMLHTTP")
                    : $.ajaxSettings.xhr();
            }
        }).done(function(response) {
            item.removeClass().addClass('item ' + action);
            cbManager.show('Dados atualizados com sucesso!');
            if (action == 'delete') {
                item.slideUp();
            }
        }).fail(function(err) {
            alert('fail');
        }).complete(function() {

        });
    });
});