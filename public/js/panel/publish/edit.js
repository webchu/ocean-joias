$(function() {
    $('[name="product[price]"]').mask('000.000.000.000.000,00', {reverse: true});
    $('.input-date').mask('99/99/9999');
    $('.km').mask("##.#00", {reverse: true, maxlength: false});

    $('select.year-model').on('change', function() {
        var year_model = parseInt($(this).val()),
            last_value = parseInt($('select.year-fab option:last').val());
        $('select.year-fab option').attr('disabled', true);
        for (var x=year_model; x>=last_value; x--) {
            $('select.year-fab option[value="' + x + '"]').removeAttr('disabled');
        }
        $('select.year-fab option[value="' + year_model + '"]').attr('selected', true);
    });

    $('.btn-choose-type').on('click', function() {
        var type = $(this).data('type');

        switch (type) {
            case 'motorcycle':
                var heightForm = $('#formAdvertiseMotorcycle').outerHeight();
                $('.forms').height(heightForm + 100);
                $('#formAdvertiseAccessory').removeClass('selected');
                $('#formAdvertiseMotorcycle').addClass('selected');


            break;

            case 'accessory':
                var heightForm = $('#formAdvertiseAccessory').outerHeight();
                $('.forms').height(heightForm + 100);
                $('#formAdvertiseMotorcycle').removeClass('selected');
                $('#formAdvertiseAccessory').addClass('selected');
            break;
        }
    });
    $('#formAdvertiseMotorcycle').validate({
        errorElement: 'span',
        rules: {
            'product[id_category]': 'required',
            'product[name]': 'required',
            'product[price]': 'required',
            'product[link]': 'required',
        },
        messages: {
            
        },
        submitHandler: function(form) {

            var product_id = $('[name="id"]', form).val();
            
            var formData = new FormData();
            if ($('[name="product_image_0"]')[0].files[0] != undefined)
                formData.append('product_image_0', $('[name="product_image_0"]')[0].files[0]);
            
            if ($('[name="product_image_1"]')[0].files[0] != undefined)
                formData.append('product_image_1', $('[name="product_image_1"]')[0].files[0]);
                
            if ($('[name="product_image_2"]')[0].files[0] != undefined)
                formData.append('product_image_2', $('[name="product_image_2"]')[0].files[0]);
                
            if ($('[name="product_image_3"]')[0].files[0] != undefined)
                formData.append('product_image_3', $('[name="product_image_3"]')[0].files[0]);
                
            if ($('[name="product_image_4"]')[0].files[0] != undefined)
                formData.append('product_image_4', $('[name="product_image_4"]')[0].files[0]);
            
            
            var other_data = $(form).serializeArray();
		    $.each(other_data,function(key,input){
		        formData.append(input.name,input.value);
		    });
            
            //var data = _.object(_.map(, _.values));
            
            
            
            
            //var data = $(form).serializeArray();
            $.ajax({
                url: '/product',
                type: 'post',
                contentType: false,
                processData: false,
                //contentType : 'application/json',
                //data: JSON.stringify(data),
                data: formData,

                //url : '/api/product/7',
                //data : JSON.stringify(formData),
                //type : 'PATCH',
                //contentType: false,
                //processData: false,
                //contentType : 'application/json',
                xhr: function() {
                    return window.XMLHttpRequest == null || new window.XMLHttpRequest().addEventListener == null 
                        ? new window.ActiveXObject("Microsoft.XMLHTTP")
                        : $.ajaxSettings.xhr();
                },
                beforeSend: function() {
                    $('body').addClass('loading');
                }
            }).done(function(response, textStatus, xhr) {
                $('#m-advertise-success').modal('show');
            }).complete(function() {
                $('body').removeClass('loading');
            });
        }
    });

    $('#formAdvertiseAccessory').validate({
        errorElement: 'span',
        rules: {
            'product[name]': 'required',
            'product[price]': 'required',
            'product[link]': 'required',
        },
        messages: {
            
        },
        submitHandler: function(form) {
            var data = _.object(_.map($(form).serializeArray(), _.values));
            $.ajax({
                url: '/product',
                type: 'post',
                contentType : 'application/json',
                data: JSON.stringify(data),
            }).done(function(response, textStatus, xhr) {
                console.log(response, textStatus, xhr);
            });
        }
    });
    
    $("#id_family").change(function () {   
        var id = $(this).val();
        $('#id_model option:not(:first)').hide();
        $('#id_model').val(0).trigger('change');
        $('#id_model option[data-family="' + id + '"]').show();
    });
});