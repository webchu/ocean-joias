ko.validation.rules.pattern.message = 'Invalid.';

ko.validation.init({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: true,
    parseInputAttributes: true,
    messageTemplate: null
}, true);

ko.validation.rules['creditCard'] = {
    getValue: function (o) {
        return (typeof o === 'function' ? o() : o);
    },
    validator: function (val, cardTypeField) {
        var self = this;

        var cctype = self.getValue(cardTypeField);
        if (!cctype) return false;
        cctype = cctype.toLowerCase();

        if (val.length < 15) {
            return (false);
        }
        var match = cctype.match(/[a-zA-Z]{2}/);
        if (!match) {
            return (false);
        }

        var number = val;
        match = number.match(/[^0-9]/);
        if (match) {
            return (false);
        }

        var fnMod10 = function (number) {
            var doubled = [];
            for (var i = number.length - 2; i >= 0; i = i - 2) {
                doubled.push(2 * number[i]);
            }
            var total = 0;
            for (var i = ((number.length % 2) == 0 ? 1 : 0) ; i < number.length; i = i + 2) {
                total += parseInt(number[i]);
            }
            for (var i = 0; i < doubled.length; i++) {
                var num = doubled[i];
                var digit;
                while (num != 0) {
                    digit = num % 10;
                    num = parseInt(num / 10);
                    total += digit;
                }
            }

            if (total % 10 == 0) {
                return (true);
            } else {
                return (false);
            }
        }

        switch (cctype) {
            case 'vc':
            case 'mc':
            case 'ae':
                //Mod 10 check
                if (!fnMod10(number)) {
                    return false;
                }
                break;
        }
        switch (cctype) {
            case 'vc':
                if (number[0] != '4' || (number.length != 13 && number.length != 16)) {
                    return false;
                }
                break;
            case 'mc':
                if (number[0] != '5' || (number.length != 16)) {
                    return false;
                }
                break;

            case 'ae':
                if (number[0] != '3' || (number.length != 15)) {
                    return false;
                }
                break;

            default:
                return false;
            }

        return (true);
    },
    message: 'Card number not valid.'
};

$(function() {
	$('.btn-login').on('click', function(e) {
		e.preventDefault();
		app.modal.open('#modal-login-failed');
	});

	/*$('.btn-finish').on('click', function(e) {
		e.preventDefault();

		if ($('#checkContract').is(':checked'))
			document.location.href = '/checkout/confirmacao';
		else
			alert('Você deve aceitar os Termos do Contrato!');
	});*/

	$('body').on('click', '.i-agree', function() {
		$('#checkContract').attr('checked', true);
		app.modal.close();
	});

	//$('.checkout .nav-tabs li:first a').trigger('click');
});

var SummaryViewModel = function() {
	var self = this;
	self.list = ko.observableArray(paymentMethods);
	self.total = ko.observable();
	self.delivery = ko.observable();

	self.payment = {
		id: ko.observable(),
		method: ko.observable(paymentMethods[0].alias),
		name: ko.observable(paymentMethods[0].name),
		condition: ko.observable(),
	};

	self.setPaymentMethod = function(data, event) {
		self.payment.id(data.id);
		self.payment.method(data.alias);
		self.payment.name(data.name);
		self.payment.condition(data.condition);
	};

	var onlyCredit = function() {
        return self.payment.method() === 'credit';
    };

    self.cards = ko.observableArray(cards);
    self.cardType = ko.observable();

	self.cardNumber = ko.observable().extend({ 
		required: {
			onlyIf: onlyCredit,
			message: 'Este campo é obrigatório'
		}
		//creditCard: self.cardType
	});
	self.cardName = ko.observable().extend({ 
		required: {
			onlyIf: onlyCredit,
			message: 'Este campo é obrigatório'
		}
	});
	// self.cardValidate = ko.observable().extend({ 
	// 	required: {
	// 		onlyIf: onlyCredit
	// 	}
	// });
	self.cardMonth = ko.observable().extend({ 
		required: {
			onlyIf: onlyCredit,
			message: 'Informe o mês'
		}
	});
	self.cardYear = ko.observable().extend({ 
		required: {
			onlyIf: onlyCredit,
			message: 'Informe o ano'
		}
	});
	self.cardCode = ko.observable().extend({ 
		required: {
			onlyIf: onlyCredit,
			message: 'Este campo é obrigatório'
		}
	});
	/*self.cardInstallment = ko.observable().extend({ 
		validation: {
			validator: function(v) {
				return v > 0;
			},
			onlyIf: onlyCredit,
			message: 'Selecione um parcelamento'
		},
	});*/

	self.cardInstallment = ko.observable('1');
	
	//self.cardInstallment
	self.installments = ko.observable(1);

	self.cardHolder = ko.observable(true);
	
	self.cardPhone = ko.observable().extend({ 
		required: {
			onlyIf: onlyCredit,
			message: 'Este campo é obrigatório'
		}
	});
	self.cardDocument = ko.observable().extend({ 
		required: {
			onlyIf: onlyCredit,
			message: 'Este campo é obrigatório'
		}
	});
	self.cardBirthDate = ko.observable().extend({ 
		required: {
			onlyIf: onlyCredit,
			message: 'Este campo é obrigatório'
		}
	});

	self.showConditions = ko.computed(function() {
		if (self.payment.method() == 'payment-slip')
			return self.payment.condition();
		else {
			var condition = self.cardInstallment();
			if (condition != undefined && condition.toLowerCase() != 'selecione') {
				var conditions = self.cardInstallment();
				return (self.cardInstallment() == 1) ? 'À vista' : 'Parcelado em ' + self.cardInstallment() + ' vezes';
			}
			else return '';
		}
		//return (self.payment.method() == 'payment-slip') ? self.payment.condition() : self.cardInstallment();
	});

	self.submit = function() {
		if (self.errors().length === 0) {
			if ($('#checkContract').is(':checked')) {
				var $form = $('.checkout form');
				if (self.payment.method() === 'credit') {
					$('.btn-finish').attr('disabled', true).html('aguarde...');
					var settings = {
				        "Forma": "CartaoCredito",
				        "Instituicao": 'Visa',
				        "Parcelas": "1",
				        "CartaoCredito": {
				            "Numero": self.cardNumber(),
				            "Expiracao": self.cardMonth() + '/' + self.cardYear(),
				            "CodigoSeguranca": self.cardCode(),
				            "Portador": {
				                "Nome": self.cardName(),
				                "DataNascimento": self.cardBirthDate(),
				                "Telefone": self.cardPhone(),
				                "Identidade": self.cardDocument()
				            }
				        }
				    };
				    MoipWidget(settings);
					/*
					$.ajax({
						url: '/checkout/process',
						data: $form.serialize(),
						type: 'post',
						beforeSend: function(){
							$('.btn-finish').attr('disabled', true);
							$.publish('/button/loading/start', $('.btn-finish'));
						}
					}).done(function(response) {
						if (response.hasOwnProperty('status') && response.status === true) {
							document.location.href = '/checkout/confirmacao';
						} else {
							alert('Ops! Ocorreu um erro na transação. Tente novamente.');
						}
					}).fail(function() {
						
					}).always(function() {
						$('.btn-finish').removeAttr('disabled');
						$.publish('/button/loading/stop', $('.btn-finish'));
					});
					*/
				} else {
					//document.location.href = '/checkout/confirmacao';
					$.ajax({
							url: '/pagamento/boleto',
							data: $form.serialize(),
							type: 'post',
							beforeSend: function(){
								$('.btn-finish').attr('disabled', true);
								$.publish('/button/loading/start', $('.btn-finish'));
							}
						}).done(function(response) {
							if (response.hasOwnProperty('status') && response.status === true) {
								document.location.href = '/checkout/confirmacao';
							} else {
								alert('Ops! Ocorreu um erro na transação. Tente novamente.');
							}
						}).fail(function() {
							
						}).always(function() {
							
						});
				}
			} else {
				alert('Você deve aceitar os Termos do Contrato!');
			}
        }
        else {
            //alert('Please check your submission.');
            self.errors.showAllMessages();
        }
	}

	$('input[name="card"]').on('keyup', function() {
		var number = $(this).val().replace(/[ ]/g, '');
		console.log(number);
		if (/^4[0-9]{12}(?:[0-9]{3})?$/.test(number)) {
			$('input[name="cardType"][value=1]').attr('checked', true)
		} else if (/^5[1-5][0-9]{14}$/.test(number)) {
			$('input[name="cardType"][value=2]').attr('checked', true)
		} else {
			//$(this).val('');
		}
	});
};



var summaryViewModel = new SummaryViewModel();
summaryViewModel.errors = ko.validation.group(summaryViewModel);

ko.applyBindings(summaryViewModel, $('.checkout')[0]);


var funcaoSucesso = function(data){
    $.ajax({
    	url: '/checkout/process',
    	type: 'post',
    	data: {
    		CodigoMoIP: data.CodigoMoIP
    	}
    }).done(function(response) {
    	if (response.status == 1)
    		document.location.href = '/checkout/confirmacao';
    	else {
    		alert('Ops! Ocorreu um erro ao processar o pagamento.');
    		$('.btn-finish').removeAttr('disabled', true).html('Finalizar compra');
    	}
    });
};

var funcaoFalha = function(data) {
    alert('Falha\n' + JSON.stringify(data));
    $('.btn-finish').removeAttr('disabled', true).html('Finalizar compra');
};