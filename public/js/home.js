$(function() {
	/*$('#search-form').on('submit', function(e) {
        //alert('post...');
    });*/

    $.getJSON('/estados_cidades.json', function (data) {
        var items = [];
        var options = '<option value="">escolha um estado</option>';    
        $.each(data, function (key, val) {
            options += '<option value="' + val.nome + '">' + val.nome + '</option>';
        });                 
        $("#estado").html(options);      
        $('#estado').multiselect( 'reload' );
        
        $("#estado").change(function () {    
            if ($(this).val() != '' && $(this).val().length > 1) {
                $('.wrapper-select-city').css('visibility', 'hidden');
            } else {          
                var options_cidades = '';
                var str = "";                   
                
                $("#estado option:selected").each(function () {
                    str += $(this).text();
                });
                
                $.each(data, function (key, val) {
                    if(val.nome == str) {                           
                        $.each(val.cidades, function (key_city, val_city) {
                            options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                        });                         
                    }
                });
                $("#cidade").html(options_cidades);
                $('#cidade').multiselect('reload');
                $('.wrapper-select-city').css('visibility', 'visible');
            }
        });
    });
});