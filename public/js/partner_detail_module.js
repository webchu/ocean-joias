angular.module('partnerApp', [])
	.controller('PartnerDetailController', ['$scope', function($scope) {
		var _this = this;
		$scope.products = [
			{
				id: 1,
				name: '10% de desconto nos dias de semana',
				discount: 10
			}, {
				id: 2,
				name: '5% de desconto nos finais de semana',
				discount: 5
			}
		];

		$scope.Calculate = function(value, discount) {
			return value - (discount/100 * value);
		}


	}]);