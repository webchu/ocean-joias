$(function() {
	$('a.delete').each(function() {
		var href = $(this).attr('href');
		$(this).attr('data-href', href);
		$(this).attr('href', '#self');
	});

	$('a.delete').on('click', function() {
		if (confirm('Deseja realmente excluir este registro?')) {
			var href = $(this).attr('data-href');
			window.location.href = href;
			return false;
		}
	});
	
	$('.manager-category .table, .manager-article .table, .manager-section .table, .manager-file .table').DataTable();
});