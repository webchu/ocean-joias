Handlebars.registerHelper('currency', function(options) {
	var price = parseFloat(options.fn(this));
	console.log(price);
	return new Handlebars.SafeString(
		'R$ ' + price.formatMoney(2, ',', '.'));
});

;(function ($, window, document, undefined) {
	cbManager = {
		show: function(message) {
			$('#callback-wrapper .message').html(message);
			$('#callback-wrapper').addClass('show');
			this.hide();
		},
		hide: function() {
			var t = window.setTimeout(function() {
				$('#callback-wrapper').removeClass('show');
			}, 3000);
		}
	};
	
	/*app = {
		getURLParameter: function(name, url) {
			return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || [, ""])[1].replace(/\+/g, '%20')) || null
		},
		getParameterByName: function(name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
				results = regex.exec(location.search);
			return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		},	
	};*/

	
	
})(jQuery, window, document);

$(function() {
	// abre modal global
	$('[data-modal]').on('click', function() {
		var modal = $(this).data('modal');
		app.modal.open(modal);
	});

	// aplica tooltip
	$('[data-toggle="tooltip"]').tooltip();

	// pop-over
	$('[data-toggle="popover"]').popover({ trigger: "hover" });	
});