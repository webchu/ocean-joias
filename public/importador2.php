<?php
if ($_POST) :
    
    $host_bd = 'bdtest-5.mysql.uhserver.com';
    $user_bd = 'bdtest_5';
    $pass_bd = '1qaz2wsx@';
    $bd      = 'bdtest_5';
    	
    $con = mysql_connect($host_bd, $user_bd, $pass_bd) or die('erro na conexao');

    $q = "INSERT INTO " . $bd . ".collection (name) VALUES ('" . $_POST['collection'] . "')";
    $r = mysql_query($q, $con) or die(mysql_error().$q);
    $collection_id = mysql_insert_id();

    $file = $_POST['csv'];
    $lines = explode("\n", $file);

    /*
    0 S.No 序号
    1 ,Code
    2 ,Picture     图片
    3 ,Style
    4 ,CZ
    5 ,GLASS
    6 ,Item
    7 ,Price/Unit 
    8 ,Qty per model
    9 ,Total
    */
    
    for ($x = 2; $x < count($lines); $x++) :
        $line = explode(",", $lines[$x]);
        
        $category = trim($line[6]);
        $q = "SELECT id FROM " . $bd . ".category WHERE name = '" . $category . "'";
        $rCheck = mysql_query($q, $con) or die(mysql_error().$q);
        
        if (mysql_num_rows($rCheck)) :
            $rsCategory = mysql_fetch_array($rCheck);
			$category_id = $rsCategory['id'];
        else :
            $q = "INSERT INTO " . $bd . ".category (name) VALUES ('" . $category . "')";    
            $r = mysql_query($q, $con) or die(mysql_error().$q);
            $category_id = mysql_insert_id();
        endif;

        $q = "INSERT INTO " . $bd . ".product (reference, name, price) VALUES ('" . $line[1] . "', '" . $line[1] . "', '" . trim(str_replace(array('$', ' ', ','), array('', '', '.'), $line[7])) . "')";
        //echo $q . '<br>';
        $r = mysql_query($q, $con) or die(mysql_error().$q);
        $product_id = mysql_insert_id();
        
        $q = "INSERT INTO " . $bd . ".product_category (product_id, category_id) VALUES (" . $product_id  . ", " . $category_id . ")";
        $r = mysql_query($q, $con) or die(mysql_error().$q);

        // associa produto x coleção
        $q = "INSERT INTO " . $bd . ".product_collection (product_id, collection_id) VALUES (" . $product_id  . ", " . $collection_id . ")";
        $r = mysql_query($q, $con) or die(mysql_error().$q);
    endfor;
    
endif;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    
    
    <link href="/css/bootstrap/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap/bootstrap-theme.min.css" media="screen" rel="stylesheet" type="text/css">

    <!-- Scripts -->
    <!--[if lt IE 9]><script type="text/javascript" src="/js/html5shiv.min.js"></script><![endif]-->
    <!--[if lt IE 9]><script type="text/javascript" src="/js/respond.min.js"></script><![endif]-->
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery.validate.min.js"></script>

</head>
<body>
    <div class="container">
        
        <form method="post">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Coleção:</label>
                        <input type="text" name="collection" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>CSV:</label>
                        <textarea name="csv" class="form-control" style="height:200px;"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-primary">Importar</button>
                </div>
            </div>
            
        </form>
    </div>
</body>
</html>