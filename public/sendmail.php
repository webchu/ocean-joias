<?php
header('Content-Type: application/json');
/* apenas dispara o envio do formulário caso exista $_POST['enviarFormulario']*/
 
if (isset($_POST['enviar']) && (isset($_POST['check']) && empty($_POST['check'])) ){
	 
	 
	/*** INÍCIO - DADOS A SEREM ALTERADOS DE ACORDO COM SUAS CONFIGURAÇÕES DE E-MAIL ***/
	 
	$enviaFormularioParaNome = 'Kelly - Orçamentos';
	$enviaFormularioParaEmail = 'orcamentos@oceanjoias.com.br';
	 
	$caixaPostalServidorNome = 'Ocean Jóias';
	$caixaPostalServidorEmail = 'orcamentos@oceanjoias.com.br';
	$caixaPostalServidorSenha = '0c3anj0ias';
	 
	/*** FIM - DADOS A SEREM ALTERADOS DE ACORDO COM SUAS CONFIGURAÇÕES DE E-MAIL ***/ 
	 
	 
	/* abaixo as veriaveis principais, que devem conter em seu formulario*/
	 
	$nome  = $_POST['nome'];
	$email = $_POST['email'];
	$regiao = $_POST['regiao'];
	//$data_interesse = $_POST['data_interesse'];
	//$como_conheceu = $_POST['como_conheceu'];
	$mensagem = $_POST['mensagem'];
	$assunto  = 'Fale conosco';
	 
	$mensagemConcatenada = 'Formulário gerado via website'.'<br/>'; 
	$mensagemConcatenada .= '-------------------------------<br/><br/>'; 
	$mensagemConcatenada .= 'Nome: '.$nome.'<br/>'; 
	$mensagemConcatenada .= 'E-mail: '.$email.'<br/>'; 
	$mensagemConcatenada .= 'Região: '.$regiao.'<br/>'; 
	//$mensagemConcatenada .= 'Data de interesse: '.$data_interesse.'<br/>'; 
	//$mensagemConcatenada .= 'Como nos conheceu: '.$como_conheceu.'<br/>'; 
	$mensagemConcatenada .= '-------------------------------<br/><br/>'; 
	$mensagemConcatenada .= 'Mensagem: "'.$mensagem.'"<br/>';
	$mensagemConcatenada .= '-------------------------------<br/><br/>';
	$mensagemConcatenada .= 'Enviado em: ' . date('d/m/Y H:i:s').'<br/>'; 
	$mensagemConcatenada .= 'IP: ' . $_SERVER['REMOTE_ADDR'];

	 
	 
	/*********************************** A PARTIR DAQUI NAO ALTERAR ************************************/ 
	 
	require_once('PHPMailer-master/PHPMailerAutoload.php');
	 
	$mail = new PHPMailer();
	 
	$mail->IsSMTP();
	$mail->SMTPAuth  = true;
	$mail->Charset   = 'utf8_decode()';
	$mail->Host  = 'smtp.'.substr(strstr($caixaPostalServidorEmail, '@'), 1);
	$mail->Port  = '587';
	$mail->Username  = $caixaPostalServidorEmail;
	$mail->Password  = $caixaPostalServidorSenha;
	$mail->From  = $caixaPostalServidorEmail;
	$mail->FromName  = utf8_decode($caixaPostalServidorNome);
	$mail->AddBCC('webchu@gmail.com', 'Chu');
	$mail->IsHTML(true);
	$mail->Subject  = utf8_decode($assunto);
	$mail->Body  = utf8_decode($mensagemConcatenada);
	 
	 
	$mail->AddAddress($enviaFormularioParaEmail,utf8_decode($enviaFormularioParaNome));
	 
	if(!$mail->Send()){
		$data = array('status' => 0, 'message' => 'Erro ao enviar formulário: '. print($mail->ErrorInfo));
	}else{
		$data = array('status' => 1, 'message' => 'Formulário enviado com sucesso!');
	} 
	echo json_encode($data); 
}