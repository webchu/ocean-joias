<?php
namespace api\V1\Rest\Product;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Manager\Model\ModelFactory;
use Manager\Model\Product\ProductModel;
use Manager\Model\Model\ModelModel;

use Application\Model\MySessionManager;

class ProductResource extends AbstractResourceListener
{
    private $_session;
    
    private $_productMapper;
    
    private $_user;
    
    protected $_model;
    
    public function __construct()
    {
        $this->_session = new MySessionManager();
        $this->_productMapper = ModelFactory::getInstance()->getProductMapper();
        $this->_model = ModelFactory::getInstance()->getModelMapper();
    }
    
    private function _checkAuth()
    {
        
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        if (!$this->_session->isAuthenticate()) {
            return new ApiProblem(401, 'Unauthorized');
        } else {
            $user = $this->_session->getUser();
            return $this->_productMapper->getProductModelsAsArray();
        }
        
        
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        if (!$this->_session->isAuthenticate()) {
            return new ApiProblem(401, 'Unauthorized');
        } else {
            $user = $this->_session->getUser();
            $product = $this->_productMapper->getProductModelByCriteria(array(
                "id" => $id,
                "id_user" => $user["id"]
            ))->current();
            if (empty($product)){
                return new ApiProblem(404, 'Product not found');
            } else {
                //echo "\n\n" . print_r($data->state, 1) . "\n\n";
                $product->setStatus($data->state);
                $this->_productMapper->saveProductModel($product);
                return true;
            }   
        }
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
