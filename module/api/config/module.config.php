<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'api\\V1\\Rest\\Product\\ProductResource' => 'api\\V1\\Rest\\Product\\ProductResourceFactory',
            'api\\V1\\Rest\\User\\UserResource' => 'api\\V1\\Rest\\User\\UserResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'api.rest.product' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/product[/:product_id]',
                    'defaults' => array(
                        'controller' => 'api\\V1\\Rest\\Product\\Controller',
                    ),
                ),
            ),
            'api.rest.user' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/user[/:user_id]',
                    'defaults' => array(
                        'controller' => 'api\\V1\\Rest\\User\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'api.rest.product',
            1 => 'api.rest.user',
        ),
    ),
    'zf-rest' => array(
        'api\\V1\\Rest\\Product\\Controller' => array(
            'listener' => 'api\\V1\\Rest\\Product\\ProductResource',
            'route_name' => 'api.rest.product',
            'route_identifier_name' => 'product_id',
            'collection_name' => 'product',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'PATCH',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'api\\V1\\Rest\\Product\\ProductEntity',
            'collection_class' => 'api\\V1\\Rest\\Product\\ProductCollection',
            'service_name' => 'product',
        ),
        'api\\V1\\Rest\\User\\Controller' => array(
            'listener' => 'api\\V1\\Rest\\User\\UserResource',
            'route_name' => 'api.rest.user',
            'route_identifier_name' => 'user_id',
            'collection_name' => 'user',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'api\\V1\\Rest\\User\\UserEntity',
            'collection_class' => 'api\\V1\\Rest\\User\\UserCollection',
            'service_name' => 'user',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'api\\V1\\Rest\\Product\\Controller' => 'HalJson',
            'api\\V1\\Rest\\User\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'api\\V1\\Rest\\Product\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'api\\V1\\Rest\\User\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'api\\V1\\Rest\\Product\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ),
            'api\\V1\\Rest\\User\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'api\\V1\\Rest\\Product\\ProductEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.product',
                'route_identifier_name' => 'product_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'api\\V1\\Rest\\Product\\ProductCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.product',
                'route_identifier_name' => 'product_id',
                'is_collection' => true,
            ),
            'api\\V1\\Rest\\User\\UserEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.user',
                'route_identifier_name' => 'user_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'api\\V1\\Rest\\User\\UserCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.user',
                'route_identifier_name' => 'user_id',
                'is_collection' => true,
            ),
        ),
    ),
);
