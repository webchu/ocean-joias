<?php
namespace service\V1\Rest\Favorite;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Manager\Model\ModelFactory;
use Manager\Model\Favorite\FavoriteModel;

use Application\Model\MySessionManager;

class FavoriteResource extends AbstractResourceListener
{

    private $_session;
    
    private $_favoriteMapper;
    
    public function __construct()
    {
        $this->_session = new MySessionManager();
        $this->_favoriteMapper = ModelFactory::getInstance()->getFavoriteMapper();
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $dataArray = json_decode(json_encode($data), true);
        $id = $dataArray['id'];
        if ($this->_session->isAuthenticate()) {
            $user = $this->_session->getUser();
            
            $favorite = $this->_favoriteMapper->getFavoriteModel($user["id"], $id);
            
            if (empty($favorite)) {
                $model = new FavoriteModel();
                $model->setIdProduct($id);
                $model->setIdUser($user["id"]);
                $model->setDateCreate(date("Y-m-d H:i:s"));
                $this->_favoriteMapper->saveFavoriteModel($model);
            } else {
                $this->_favoriteMapper->deleteFavoriteModel($favorite);
            }
            return array("status" => true);
        } else {
            return new ApiProblem(401, 'Unauthorized');
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        if ($this->_session->isAuthenticate()) {
            $user = $this->_session->getUser();
            
            $favorite = $this->_favoriteMapper->getFavoriteModel($user["id"], $id);
            
            if (empty($favorite)) {
                return array("status" => false);
            } else {
                return array("status" => true);
            }
        } else {
            return new ApiProblem(401, 'Unauthorized');
        }
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        if ($this->_session->isAuthenticate()) {
            $user = $this->_session->getUser();
            
            $favorites = $this->_favoriteMapper->getFavoriteModelsByIdUser($user["id"]);
            
            if (empty($favorites)) {
                return array("status" => true, "favorites" => array());
            } else {
                foreach($favorites as $key => $fav){
                    $fav["id_product"] = $key;
                    $favs[] = $fav;
                }
                echo print_r(json_encode($favs), 1); exit;
                //return $favorites->toArray();
            }
        } else {
            return new ApiProblem(401, 'Unauthorized');
        }
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
