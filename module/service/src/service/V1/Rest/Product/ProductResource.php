<?php
namespace service\V1\Rest\Product;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Manager\Model\ModelFactory;
use Manager\Model\Product\ProductModel;
use Manager\Model\ProductProperty\ProductPropertyModel;

use Application\Model\MySessionManager;

use Manager\Model\UploadModel;
use Manager\Model\ProductImage\ProductImageModel;

class ProductResource extends AbstractResourceListener
{
    private $_session;
    
    private $_productMapper;
    
    public function __construct()
    {
        $this->_session = new MySessionManager();
        $this->_productMapper = ModelFactory::getInstance()->getProductMapper();
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        
        //echo "1 \n\n" . print_r($files, 1) . "\n\n";
        //exit;
        $dataArray = json_decode(json_encode($data), true);
        //echo "2 \n\n" . print_r($dataArray, 1) . "\n\n";
        //return new ApiProblem(400, 'Invalid Fields');
        //exit;
        if ($this->_session->isAuthenticate()) {
            if ($this->_checkRequiredFields($dataArray)) {
                $dataArray['product']["status"] = 1;
                $user = $this->_session->getUser();
                $dataArray['product']["id_user"] = $user["id"];
                //$dataArray['product']["id_model"] = 1;
                $model = new ProductModel();
                $model->exchangeArray($dataArray['product']);
                try {
                    $model->setPrice(str_replace('.', '', $model->getPrice()));
                    if (isset($dataArray["id"]) && $dataArray["id"] > 0) {
                        $productLoaded = $this->_productMapper->getProductModel($dataArray["id"]);
                        if ($productLoaded->getIdUser() != $user["id"]) {
                            throw new \Exception("Invalid Product");
                        } else {
                            $model->setId($dataArray["id"]);
                        }
                    }
                    //echo " \n\n" . print_r($model->toArray(), 1) . "\n\n";
                    //exit;
                    $model->setDateCreate(date("Y-m-d H:i:s"));
                    $productId = $this->_productMapper->saveProductModel($model);
                    $upload = new \Zend\File\Transfer\Transfer();
                    $files  = $upload->getFileInfo();
                    
                    //echo " \n\n" . print_r($files, 1) . "\n\n";
                    //exit;
                    $this->_session->setProduct($productId);
                    if (isset($files) && is_array($files) && !empty($files)){
                        $this->_sendFilesToS3($files, $productId);
                    }
                    //echo "<pre>" . print_r($dataArray, 1); exit;
                    $this->_saveProperties($productId, $dataArray['product_property']);
                } catch (\Exception $e) {
                    return new ApiProblem(400, 'Exception: ' . $e->getMessage());
                }
                //exit;
                return array("status" => true);
            } else {
                return new ApiProblem(400, 'Invalid Fields');
            }
        } else {
            return new ApiProblem(401, 'Unauthorized');
        }
    }
    
    private function _sendFilesToS3(array $files, $productId)
    {
        $mapper = ModelFactory::getInstance()->getProductImageMapper();
        foreach($files as $file) {
            $upload = new UploadModel();
            $path = $upload->sendToS3($file, $productId, "product");
            $model = new ProductImageModel();
            $model->exchangeArray(array(
                "id_product" => $productId,
                "url" => $upload->getPath(),
            ));
            $mapper->saveProductImageModel($model);
        }
    }
    
    private function _saveProperties($productId, array $properties)
    {
        //echo "PRO = <pre>" . print_r($properties, 1); exit;
        
        if (!empty($productId) && $productId > 0){
            $mapper = ModelFactory::getInstance()->getProductPropertyMapper();
            foreach($properties as $propertie) {
                $model = new ProductPropertyModel();
                $model->exchangeArray($propertie);
                $model->setIdProduct($productId);
                $mapper->saveProductPropertyModel($model);
            }
        } else {
            return new ApiProblem(400, 'Invalid Product Id');
        }
    }
    
    private function _checkRequiredFields($dataArray)
    {
        echo "\n\n <pre>" . print_r($dataArray['product']['id_type'], 1) . "</pre>\n\n";
        return new ApiProblem(400, 'Invalid Product Id');
        exit;
        if ($dataArray['product']['id_type'] == 1){
            if (isset($dataArray['product']['id_model'])){
                return true;
            }
        }
        if ($dataArray['product']['id_type'] == 2){
            return true;
        }
        return false;
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        if ($this->_session->isAuthenticate()) {
            return $this->_productMapper->getCategoryModels()->toArray();
        }  else {
            return new ApiProblem(401, 'Unauthorized');
        }
        
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        //return array("status" => true);
        $dataArray = json_decode(json_encode($data), true);
        echo "PATH  \n\n" . print_r($dataArray, 1) . "\n\n";
        return new ApiProblem(400, 'Invalid Fields');
        exit;
        if ($this->_session->isAuthenticate()) {
            if ($this->_checkRequiredFields($dataArray)) {
                $dataArray['product']["status"] = 1;
                $user = $this->_session->getUser();
                $dataArray['product']["id_user"] = $user["id"];
                $dataArray['product']["id_model"] = 1;
                $model = new ProductModel();
                $model->exchangeArray($dataArray['product']);
                try {
                    $model->setPrice(str_replace('.', '', $model->getPrice()));
                    $productId = $this->_productMapper->saveProductModel($model);
                    
                    $upload = new \Zend\File\Transfer\Transfer();
                    $files  = $upload->getFileInfo();
                    
                    //echo " \n\n" . print_r($files, 1) . "\n\n";
                    //exit;
                    
                    if (isset($files) && is_array($files) && !empty($files)){
                        $this->_sendFilesToS3($files, $productId);
                    }
                    
                    $this->_saveProperties($productId, $dataArray['product_property']);
                } catch (\Exception $e) {
                    return new ApiProblem(400, 'Exception: ' . $e->getMessage());
                }
                //exit;
                return array("status" => true);
            } else {
                return new ApiProblem(400, 'Invalid Fields');
            }
        } else {
            return new ApiProblem(401, 'Unauthorized');
        }
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
