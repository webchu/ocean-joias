<?php
namespace service\V1\Rest\User;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Manager\Model\ModelFactory;
use Manager\Model\User\UserModel;


class UserResource extends AbstractResourceListener
{
    
    protected $_userMapper;
    
    public function __construct()
    {
        $this->_userMapper = ModelFactory::getInstance()->getUserMapper();
    }
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $userArray = json_decode(json_encode($data), true);
        if ($this->_checkRequiredFields($userArray)) {
            $model = new UserModel();
            $model->exchangeArray($userArray);
            $this->_userMapper->saveUserModel($model);
            //@todo: Enviar o e-mail solicitdo que clique para ativar
            $this->_sendEmailUser($model);
            return array("status" => true);
        } else {
            return new ApiProblem(400, 'Invalid Fields');
        }
    }
    
    private function _sendEmailUser($modelUser)
    {
        //Aqui envio o e-mail
    }
    
    private function _checkRequiredFields($userArray)
    {
        /*
        @TODO: Adicionar validacao
        (
            [name] => Giuseppe
            [lastName] => Lopes
            [phone] => (51) 3322-6314
            [cellphone] => (51) 3322-6314
            [document] => 821.563.870-87
            [email] => giuseppelopes@gmail.com
            [password] => 102030
            [password2] => 102030
            [postalcode] => 91540-220
            [address] => Rua B
            [city] => Porto Alegre
            [state] => RS
            [message] => 
        )
        */
        if (isset($userArray['name'])) {
            return true;
        }
        return false;
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
