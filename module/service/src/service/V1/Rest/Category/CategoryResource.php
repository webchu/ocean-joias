<?php
namespace service\V1\Rest\Category;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Manager\Model\ModelFactory;
use Manager\Model\Category\CategoryModel;

use Application\Model\MySessionManager;

class CategoryResource extends AbstractResourceListener
{
    private $_session;
    
    private $_categoryMapper;
    public function __construct()
    {
        $this->_session = new MySessionManager();
        $this->_categoryMapper = ModelFactory::getInstance()->getCategoryMapper();
    }
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        // echo "\n\t" . print_r((array)$data, 1) . "\n";
        // exit;
        if ($this->_session->isAuthenticate()) {
            if (isset($data->name, $data->description) && !empty($data->name) && !empty($data->description)) {
                $model = new CategoryModel();
                $model->exchangeArray((array)$data);
                $model->setDateCreate(date("Y-m-d H:i:s"));
                $this->_categoryMapper->saveCategoryModel($model);
                return array("status" => true);
            } else {
                return new ApiProblem(400, 'Invalid Fields');
            }
        } else {
            return new ApiProblem(401, 'Unauthorized');
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        if ($this->_session->isAuthenticate()) {
            return $this->_categoryMapper->getCategoryModels()->toArray();
        }  else {
            return new ApiProblem(401, 'Unauthorized');
        }
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
