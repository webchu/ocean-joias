<?php
return array(
    'router' => array(
        'routes' => array(
            'service.rest.login' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/login[/:login_id]',
                    'defaults' => array(
                        'controller' => 'service\\V1\\Rest\\Login\\Controller',
                    ),
                ),
            ),
            'service.rest.category' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/category[/:category_id]',
                    'defaults' => array(
                        'controller' => 'service\\V1\\Rest\\Category\\Controller',
                    ),
                ),
            ),
            'service.rest.product' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/product[/:product_id]',
                    'defaults' => array(
                        'controller' => 'service\\V1\\Rest\\Product\\Controller',
                    ),
                ),
            ),
            'service.rest.user' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user[/:user_id]',
                    'defaults' => array(
                        'controller' => 'service\\V1\\Rest\\User\\Controller',
                    ),
                ),
            ),
            'service.rest.favorite' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/favorite[/:favorite_id]',
                    'defaults' => array(
                        'controller' => 'service\\V1\\Rest\\Favorite\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            1 => 'service.rest.login',
            2 => 'service.rest.category',
            3 => 'service.rest.product',
            0 => 'service.rest.user',
            4 => 'service.rest.favorite',
        ),
    ),
    'zf-rest' => array(
        'service\\V1\\Rest\\Login\\Controller' => array(
            'listener' => 'service\\V1\\Rest\\Login\\LoginResource',
            'route_name' => 'service.rest.login',
            'route_identifier_name' => 'login_id',
            'collection_name' => 'login',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'service\\V1\\Rest\\Login\\LoginEntity',
            'collection_class' => 'service\\V1\\Rest\\Login\\LoginCollection',
            'service_name' => 'Login',
        ),
        'service\\V1\\Rest\\Category\\Controller' => array(
            'listener' => 'service\\V1\\Rest\\Category\\CategoryResource',
            'route_name' => 'service.rest.category',
            'route_identifier_name' => 'category_id',
            'collection_name' => 'category',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'service\\V1\\Rest\\Category\\CategoryEntity',
            'collection_class' => 'service\\V1\\Rest\\Category\\CategoryCollection',
            'service_name' => 'category',
        ),
        'service\\V1\\Rest\\Product\\Controller' => array(
            'listener' => 'service\\V1\\Rest\\Product\\ProductResource',
            'route_name' => 'service.rest.product',
            'route_identifier_name' => 'product_id',
            'collection_name' => 'product',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'service\\V1\\Rest\\Product\\ProductEntity',
            'collection_class' => 'service\\V1\\Rest\\Product\\ProductCollection',
            'service_name' => 'product',
        ),
        'service\\V1\\Rest\\User\\Controller' => array(
            'listener' => 'service\\V1\\Rest\\User\\UserResource',
            'route_name' => 'service.rest.user',
            'route_identifier_name' => 'user_id',
            'collection_name' => 'user',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'service\\V1\\Rest\\User\\UserEntity',
            'collection_class' => 'service\\V1\\Rest\\User\\UserCollection',
            'service_name' => 'User',
        ),
        'service\\V1\\Rest\\Favorite\\Controller' => array(
            'listener' => 'service\\V1\\Rest\\Favorite\\FavoriteResource',
            'route_name' => 'service.rest.favorite',
            'route_identifier_name' => 'favorite_id',
            'collection_name' => 'favorite',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'service\\V1\\Rest\\Favorite\\FavoriteEntity',
            'collection_class' => 'service\\V1\\Rest\\Favorite\\FavoriteCollection',
            'service_name' => 'Favorite',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'service\\V1\\Rest\\Login\\Controller' => 'HalJson',
            'service\\V1\\Rest\\Category\\Controller' => 'HalJson',
            'service\\V1\\Rest\\Product\\Controller' => 'HalJson',
            'service\\V1\\Rest\\User\\Controller' => 'HalJson',
            'service\\V1\\Rest\\Favorite\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'service\\V1\\Rest\\Login\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'service\\V1\\Rest\\Category\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'service\\V1\\Rest\\Product\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'service\\V1\\Rest\\User\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'service\\V1\\Rest\\Favorite\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'service\\V1\\Rest\\Login\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/json',
            ),
            'service\\V1\\Rest\\Category\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/json',
            ),
            'service\\V1\\Rest\\Product\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/json',
                2 => 'application/x-www-form-urlencoded',
            ),
            'service\\V1\\Rest\\User\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/json',
            ),
            'service\\V1\\Rest\\Favorite\\Controller' => array(
                0 => 'application/vnd.service.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'service\\V1\\Rest\\Login\\LoginEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.login',
                'route_identifier_name' => 'login_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'service\\V1\\Rest\\Login\\LoginCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.login',
                'route_identifier_name' => 'login_id',
                'is_collection' => true,
            ),
            'service\\V1\\Rest\\Category\\CategoryEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.category',
                'route_identifier_name' => 'category_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'service\\V1\\Rest\\Category\\CategoryCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.category',
                'route_identifier_name' => 'category_id',
                'is_collection' => true,
            ),
            'service\\V1\\Rest\\Product\\ProductEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.product',
                'route_identifier_name' => 'product_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'service\\V1\\Rest\\Product\\ProductCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.product',
                'route_identifier_name' => 'product_id',
                'is_collection' => true,
            ),
            'service\\V1\\Rest\\User\\UserEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.user',
                'route_identifier_name' => 'user_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'service\\V1\\Rest\\User\\UserCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.user',
                'route_identifier_name' => 'user_id',
                'is_collection' => true,
            ),
            'service\\V1\\Rest\\Favorite\\FavoriteEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.favorite',
                'route_identifier_name' => 'favorite_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'service\\V1\\Rest\\Favorite\\FavoriteCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'service.rest.favorite',
                'route_identifier_name' => 'favorite_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-apigility' => array(
        'db-connected' => array(),
    ),
    'zf-content-validation' => array(),
    'input_filter_specs' => array(
        'service\\V1\\Rest\\User\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '150',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'lastName',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '150',
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'document',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '15',
                        ),
                    ),
                ),
            ),
            3 => array(
                'name' => 'address',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '65535',
                        ),
                    ),
                ),
            ),
            4 => array(
                'name' => 'city',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '150',
                        ),
                    ),
                ),
            ),
            5 => array(
                'name' => 'state',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '35',
                        ),
                    ),
                ),
            ),
            6 => array(
                'name' => 'postalcode',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '35',
                        ),
                    ),
                ),
            ),
            7 => array(
                'name' => 'phone',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '20',
                        ),
                    ),
                ),
            ),
            8 => array(
                'name' => 'email',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '128',
                        ),
                    ),
                ),
            ),
            9 => array(
                'name' => 'password',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '128',
                        ),
                    ),
                ),
            ),
            10 => array(
                'name' => 'status',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(),
            ),
            11 => array(
                'name' => 'dateCreate',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'service\\V1\\Rest\\Alert\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '128',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'id_user',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbRecordExists',
                        'options' => array(
                            'adapter' => 'api-service',
                            'table' => 'user',
                            'field' => 'id',
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'status',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(),
            ),
            3 => array(
                'name' => 'dateCreate',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'service\\V1\\Rest\\Product\\Validator' => array(
            0 => array(
                'name' => 'id_type',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(),
            ),
            1 => array(
                'name' => 'id_user',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbRecordExists',
                        'options' => array(
                            'adapter' => 'api-service',
                            'table' => 'user',
                            'field' => 'id',
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'name',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '200',
                        ),
                    ),
                ),
            ),
            3 => array(
                'name' => 'price',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            4 => array(
                'name' => 'status',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(),
            ),
            5 => array(
                'name' => 'dateCreate',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'service\\V1\\Rest\\Login\\LoginResource' => 'service\\V1\\Rest\\Login\\LoginResourceFactory',
            'service\\V1\\Rest\\Category\\CategoryResource' => 'service\\V1\\Rest\\Category\\CategoryResourceFactory',
            'service\\V1\\Rest\\Product\\ProductResource' => 'service\\V1\\Rest\\Product\\ProductResourceFactory',
            'service\\V1\\Rest\\User\\UserResource' => 'service\\V1\\Rest\\User\\UserResourceFactory',
            'service\\V1\\Rest\\Favorite\\FavoriteResource' => 'service\\V1\\Rest\\Favorite\\FavoriteResourceFactory',
        ),
    ),
);
