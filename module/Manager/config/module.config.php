<?php
return array(
    'controllers' => array(
         'invokables' => array(
            'Manager\Controller\User' => 'Manager\Controller\UserController',
            'Manager\Controller\Product' => 'Manager\Controller\ProductController',
            'Manager\Controller\Category' => 'Manager\Controller\CategoryController',
            'Manager\Controller\Collection' => 'Manager\Controller\CollectionController',
            'Manager\Controller\Budget' => 'Manager\Controller\BudgetController',
         ),
     ),
     
    'router' => array(
        'routes' => array(
            'manager' => array(
                'type'    => 'segment',
                'options' => array(
                     'route'    => '/manager[/:controller[/:action[/:id[/]]]]',
                     'defaults' => array(
                         '__NAMESPACE__' => 'Manager\Controller',
                         'controller'    => 'Place',
                         'action'        => 'index',
                     ),
                     'constraints' => [
                         'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'            => '[0-9]+',
                     ]
                 ),
            ),
        ),
    ),
     
    'view_manager' => array(
        'template_path_stack' => array(
            'manager' => __DIR__ . '/../view',
        ),
        'template_map' => array(
            'manager/layout' => __DIR__ . '/../view/layout/layout.phtml',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    
     'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'pt_BR',
        'translation_file_patterns' => array(
            array(
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language/array',
                'pattern'  => '%s.php',
                'text_domain' => __NAMESPACE__,
            ),
        ),
    ),
    
    'view_helpers' => array(
        'invokables'=> array(
            'iframe_helper' => 'Manager\View\Helper\Iframehelper'
        )
    ),
);