<?php
namespace Manager;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\Router\Http\RouteMatch;

class Module
{
    //protected $whitelist = array('zfcuser/login', 'home', 'detail', 'application', 'send');
    
    public function onBootstrap(\Zend\Mvc\MvcEvent $e)
    {
        $app = $e->getApplication();
        $em  = $app->getEventManager();
        $sm  = $app->getServiceManager();

        Model\ModelFactory::getInstance()->setServiceLocator($sm);

        $translator = $sm->get('translator');
        $translator->setLocale('pt_BR')
            ->setFallbackLocale('pt_BR');

        // $list = $this->whitelist;
        // $auth = $sm->get('zfcuser_auth_service');

        // $em->attach(MvcEvent::EVENT_ROUTE, function($e) use ($list, $auth) {
        //     $match = $e->getRouteMatch();

        //     // No route match, this is a 404
        //     if (!$match instanceof RouteMatch) {
        //         return;
        //     }

        //     // Route is whitelisted
        //     $name = $match->getMatchedRouteName();
        //     if (in_array($name, $list)) {
        //         return;
        //     }

        //     // User is authenticated
        //     if ($auth->hasIdentity()) {
        //         return;
        //     }

        //     // Redirect to the user login page, as an example
        //     $router   = $e->getRouter();
        //     $url      = $router->assemble(array(), array(
        //         'name' => 'zfcuser/login'
        //     ));

        //     $response = $e->getResponse();
        //     $response->getHeaders()->addHeaderLine('Location', $url);
        //     $response->setStatusCode(302);

        //     return $response;
        // }, -100);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function init(ModuleManager $mm)
    {
        $mm->getEventManager()->getSharedManager()->attach(__NAMESPACE__,
        'dispatch', function($e) {
            $e->getTarget()->layout('manager/layout');
        });
    }
}
