<?php

namespace Manager\Model\ProductCollection;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

use Manager\Model\ModelFactory;

class ProductCollectionMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getProductCollectionModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }
    
    public function getProdutosPorColecao($collection_id)
    {
        $sql = "SELECT 
            p.id, p.name, p.style, p.reference, p.price, c.name as category
        FROM
            product p
            JOIN    product_collection pc ON pc.product_id = p.id
            LEFT JOIN product_category pca ON pca.product_id = p.id
            LEFT JOIN category c ON c.id = pca.category_id
        WHERE
            pc.collection_id =" . (int) $collection_id;
        
        //$resultSet = $this->tableGateway->getAdapter()->query($sql, array(5));
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }


    /**
     *
     * @return resultset
     */
    public function getProductCollectionModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->order('id DESC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return ProductCollectionModel
     */
    public function getProductCollectionModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param ProductCollectionModel $model
     */
    public function saveProductCollectionModel(ProductCollectionModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param ProductCollectionModel|int $model
     */
    public function deleteProductCollectionModel($model)
    {
        if ($model instanceof ProductCollectionModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ProductCollectionModel
     */
    public function getProductCollectionModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getProductCollectionModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getProductCollectionModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getProductCollectionModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}