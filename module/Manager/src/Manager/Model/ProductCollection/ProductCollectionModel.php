<?php

namespace Manager\Model\ProductCollection;

class ProductCollectionModel
{

    protected $data = array(
        'collection_id' => '',
        'product_id' => '',
        'id' => NULL,
    );

    public function getCategoryId()
    {
        return $this->data['category_id'];
    }

    public function setCategoryId($categoryId)
    {
        $this->data['category_id'] = $categoryId;
    }
    public function getProductId()
    {
        return $this->data['product_id'];
    }

    public function setProductId($productId)
    {
        $this->data['product_id'] = $productId;
    }
    public function getId()
    {
        return $this->data['id'];
    }

    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    public function exchangeArray($data)
    {
        foreach ($data as $key => $value)
        {
            if (!array_key_exists($key, $this->data)) {
                continue;//throw new \Exception("$key field does not exist in " . __CLASS__);
            }
            if ($key == "dateCreate") {
                $this->data[$key] = date("Y-m-d H:i:s");
            } else {
                $this->data[$key] = $value;
            }
        }
    }

    public function toArray()
    {
        return $this->data;
    }
    
    public function getArrayCopy()
    {
        return $this->data;
    }
    
    public function getMyName()
    {
        return strtolower("ProductCollection");
    }
}