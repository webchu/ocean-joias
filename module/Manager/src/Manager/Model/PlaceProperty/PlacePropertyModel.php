<?php

namespace Manager\Model\PlaceProperty;

class PlacePropertyModel
{

    protected $data = array(
        'id_place' => '',
        'key' => '',
        'value' => '',
        'label' => '',
        'type' => '',
        'id' => NULL,
    );

    public function getIdPlace()
    {
        return $this->data['id_place'];
    }

    public function setIdPlace($idPlace)
    {
        $this->data['id_place'] = $idPlace;
    }
    public function getKey()
    {
        return $this->data['key'];
    }

    public function setKey($key)
    {
        $this->data['key'] = $key;
    }
    public function getValue()
    {
        return $this->data['value'];
    }

    public function setValue($value)
    {
        $this->data['value'] = $value;
    }
    public function getLabel()
    {
        return $this->data['label'];
    }

    public function setLabel($label)
    {
        $this->data['label'] = $label;
    }
    public function getType()
    {
        return $this->data['type'];
    }

    public function setType($type)
    {
        $this->data['type'] = $type;
    }
    public function getId()
    {
        return $this->data['id'];
    }

    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    public function exchangeArray($data)
    {
        foreach ($data as $key => $value)
        {
            if (!array_key_exists($key, $this->data)) {
                continue;//throw new \Exception("$key field does not exist in " . __CLASS__);
            }
            if ($key == "dateCreate") {
                $this->data[$key] = date("Y-m-d H:i:s");
            } else {
                $this->data[$key] = $value;
            }
        }
    }

    public function toArray()
    {
        return $this->data;
    }
    
    public function getArrayCopy()
    {
        return $this->data;
    }
    
    public function getMyName()
    {
        return strtolower("PlaceProperty");
    }
}