<?php

namespace Manager\Model\PlaceProperty;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

class PlacePropertyMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getPlacePropertyModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }


    /**
     *
     * @return resultset
     */
    public function getPlacePropertyModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->order('id DESC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return PlacePropertyModel
     */
    public function getPlacePropertyModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param PlacePropertyModel $model
     */
    public function savePlacePropertyModel(PlacePropertyModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param PlacePropertyModel|int $model
     */
    public function deletePlacePropertyModel($model)
    {
        if ($model instanceof PlacePropertyModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $idPlace
     * @return PlacePropertyModel
     */
    public function getPlacePropertyModelsByIdPlace($idPlace)
    {
        return $this->tableGateway->select(array('id_place' => $idPlace));
    }


    /**
     *
     * @param mixed $idPlace
     * @return ResultSet
     */
    public function getPlacePropertyModelSetByIdPlace($idPlace)
    {
        return $this->tableGateway->select(array('id_place' => $idPlace));
    }

    /**
     *
     * @param mixed $key
     * @return PlacePropertyModel
     */
    public function getPlacePropertyModelsByKey($key)
    {
        return $this->tableGateway->select(array('key' => $key));
    }


    /**
     *
     * @param mixed $key
     * @return ResultSet
     */
    public function getPlacePropertyModelSetByKey($key)
    {
        return $this->tableGateway->select(array('key' => $key));
    }

    /**
     *
     * @param mixed $id
     * @return PlacePropertyModel
     */
    public function getPlacePropertyModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getPlacePropertyModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getPlacePropertyModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getPlacePropertyModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}