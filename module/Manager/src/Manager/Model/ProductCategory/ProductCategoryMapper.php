<?php

namespace Manager\Model\ProductCategory;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

use Manager\Model\ModelFactory;

class ProductCategoryMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getProdutosPorCategoria($category_id)
    {
        $sql = "SELECT 
            p.id, p.name, p.style, p.reference, p.price, c.id as category_id, c.name as category
        FROM
            product p
            JOIN product_category pca ON pca.product_id = p.id
            JOIN category c ON c.id = pca.category_id
        WHERE
            pca.category_id =" . (int) $category_id;

        $sql .= " AND p.id NOT IN (SELECT pc.id FROM collection c JOIN product_collection pc on pc.collection_id = c.id WHERE c.status = 0 )";
        
        //$resultSet = $this->tableGateway->getAdapter()->query($sql, array(5));
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }
    
    public function excluirSelecionados($ids)
    {
        $sql = "DELETE FROM product_category WHERE product_id IN (" . implode(',', $ids) . ")";
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return;
    }

    public function getProductCategoryModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }


    /**
     *
     * @return resultset
     */
    public function getProductCategoryModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->order('id DESC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return ProductCategoryModel
     */
    public function getProductCategoryModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param ProductCategoryModel $model
     */
    public function saveProductCategoryModel(ProductCategoryModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param ProductCategoryModel|int $model
     */
    public function deleteProductCategoryModel($model)
    {
        if ($model instanceof ProductCategoryModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ProductCategoryModel
     */
    public function getProductCategoryModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getProductCategoryModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getProductCategoryModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getProductCategoryModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}