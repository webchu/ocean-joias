<?php

namespace Manager\Model\Category;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

use Manager\Model\ModelFactory;

class CategoryMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getCategoryModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }


    /**
     *
     * @return resultset
     */
    public function getCategoryModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->where('status = 1');
             $select->order('name ASC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return CategoryModel
     */
    public function getCategoryModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param CategoryModel $model
     */
    public function saveCategoryModel(CategoryModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param CategoryModel|int $model
     */
    public function deleteCategoryModel($model)
    {
        if ($model instanceof CategoryModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return CategoryModel
     */
    public function getCategoryModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getCategoryModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getCategoryModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getCategoryModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}