<?php

namespace Manager\Model\BudgetItems;

class BudgetItemsModel
{

    protected $data = array(
        'budget_id' => NULL,
        'product_id' => NULL,
        'quantity' => 0,
        'id' => NULL,
    );

    public function getBudgetId() { return $this->data['budget_id']; }
    public function setBudgetId($budget_id) { $this->data['budget_id'] = $budget_id; }

    public function getProductId() { return $this->data['product_id']; }
    public function setProductId($product_id) { $this->data['product_id'] = $product_id; }

    public function getQuantity() { return $this->data['quantity']; }
    public function setQuantity($quantity) { $this->data['quantity'] = $quantity; }

    public function getId()
    {
        return $this->data['id'];
    }
    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    public function exchangeArray($data)
    {
        foreach ($data as $key => $value)
        {
            if (!array_key_exists($key, $this->data)) {
                continue;//throw new \Exception("$key field does not exist in " . __CLASS__);
            }
            if ($key == "dateCreate") {
                $this->data[$key] = date("Y-m-d H:i:s");
            } else {
                $this->data[$key] = $value;
            }
        }
    }

    public function toArray()
    {
        return $this->data;
    }
    
    public function getArrayCopy()
    {
        return $this->data;
    }
    
    public function getMyName()
    {
        return strtolower("BudgetItems");
    }
}