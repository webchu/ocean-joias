<?php

namespace Manager\Model\BudgetItems;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

use Manager\Model\ModelFactory;

class BudgetItemsMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getBudgetItemss($ids)
    {
        $sql = "SELECT *
        FROM
            budgetItems
        WHERE
            id IN (" . $ids . ")";
        
        //$resultSet = $this->tableGateway->getAdapter()->query($sql, array(5));
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }

    public function getProdutosRandom($limit = 6)
    {
        $sql = "SELECT 
            p.id, p.name, p.style, p.reference, c.id as category_id, c.name as category
        FROM
            BudgetItems p
            JOIN BudgetItems_category pca ON pca.BudgetItems_id = p.id
            JOIN category c ON c.id = pca.category_id
        WHERE
            1=1
        ORDER BY RAND()
        LIMIT " . $limit;
        
        //$resultSet = $this->tableGateway->getAdapter()->query($sql, array(5));
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }

    public function getBudgetItemsModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }


    /**
     *
     * @return resultset
     */
    public function getBudgetItemsModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->order('id DESC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return BudgetItemsModel
     */
    public function getBudgetItemsModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param BudgetItemsModel $model
     */
    public function saveBudgetItemsModel(BudgetItemsModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param BudgetItemsModel|int $model
     */
    public function deleteBudgetItemsModel($model)
    {
        if ($model instanceof BudgetItemsModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return BudgetItemsModel
     */
    public function getBudgetItemsModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getBudgetItemsModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getBudgetItemsModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getBudgetItemsModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}