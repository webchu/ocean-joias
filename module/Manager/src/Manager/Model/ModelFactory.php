<?php

namespace Manager\Model;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ModelFactory implements ServiceLocatorAwareInterface
{

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var \Manager\Model\ModelFactory
     */
    static protected $instance;

    /**
     * @return \Manager\Model\ModelFactory
     */
    static public function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    private function __construct() {}

    private function __clone() {}

    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function getBudgetItemsTableGateway()
    {
        $dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new \Manager\Model\BudgetItems\BudgetItemsModel());
        return new \Zend\Db\TableGateway\TableGateway('budget_items', $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * @return \Manager\Model\BudgetItems\BudgetItemsMapper
     */
    public function getBudgetItemsMapper()
    {
        $tableGateway = $this->getBudgetItemsTableGateway();
        $mapper = new \Manager\Model\BudgetItems\BudgetItemsMapper($tableGateway);
        $mapper->setServiceLocator($this->serviceLocator);
        return $mapper;
    }


    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function getBudgetTableGateway()
    {
        $dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new \Manager\Model\Budget\BudgetModel());
        return new \Zend\Db\TableGateway\TableGateway('budget', $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * @return \Manager\Model\Budget\BudgetMapper
     */
    public function getBudgetMapper()
    {
        $tableGateway = $this->getBudgetTableGateway();
        $mapper = new \Manager\Model\Budget\BudgetMapper($tableGateway);
        $mapper->setServiceLocator($this->serviceLocator);
        return $mapper;
    }



    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function getProductTableGateway()
    {
        $dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new \Manager\Model\Product\ProductModel());
        return new \Zend\Db\TableGateway\TableGateway('product', $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * @return \Manager\Model\Product\ProductMapper
     */
    public function getProductMapper()
    {
        $tableGateway = $this->getProductTableGateway();
        $mapper = new \Manager\Model\Product\ProductMapper($tableGateway);
        $mapper->setServiceLocator($this->serviceLocator);
        return $mapper;
    }
    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function getCategoryTableGateway()
    {
        $dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new \Manager\Model\Category\CategoryModel());
        return new \Zend\Db\TableGateway\TableGateway('category', $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * @return \Manager\Model\Category\CategoryMapper
     */
    public function getCategoryMapper()
    {
        $tableGateway = $this->getCategoryTableGateway();
        $mapper = new \Manager\Model\Category\CategoryMapper($tableGateway);
        $mapper->setServiceLocator($this->serviceLocator);
        return $mapper;
    }
    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function getCollectionTableGateway()
    {
        $dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new \Manager\Model\Collection\CollectionModel());
        return new \Zend\Db\TableGateway\TableGateway('collection', $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * @return \Manager\Model\Collection\CollectionMapper
     */
    public function getCollectionMapper()
    {
        $tableGateway = $this->getCollectionTableGateway();
        $mapper = new \Manager\Model\Collection\CollectionMapper($tableGateway);
        $mapper->setServiceLocator($this->serviceLocator);
        return $mapper;
    }
    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function getProductCategoryTableGateway()
    {
        $dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new \Manager\Model\ProductCategory\ProductCategoryModel());
        return new \Zend\Db\TableGateway\TableGateway('product_category', $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * @return \Manager\Model\ProductCategory\ProductCategoryMapper
     */
    public function getProductCategoryMapper()
    {
        $tableGateway = $this->getProductCategoryTableGateway();
        $mapper = new \Manager\Model\ProductCategory\ProductCategoryMapper($tableGateway);
        $mapper->setServiceLocator($this->serviceLocator);
        return $mapper;
    }
    
    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function getProductCollectionTableGateway()
    {
        $dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new \Manager\Model\ProductCollection\ProductCollectionModel());
        return new \Zend\Db\TableGateway\TableGateway('product_collection', $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * @return \Manager\Model\ProductCollection\ProductCollectionMapper
     */
    public function getProductCollectionMapper()
    {
        $tableGateway = $this->getProductCollectionTableGateway();
        $mapper = new \Manager\Model\ProductCollection\ProductCollectionMapper($tableGateway);
        $mapper->setServiceLocator($this->serviceLocator);
        return $mapper;
    }
    
    
    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function getUserTableGateway()
    {
        $dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new \Manager\Model\User\UserModel());
        return new \Zend\Db\TableGateway\TableGateway('user', $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * @return \Manager\Model\User\UserMapper
     */
    public function getUserMapper()
    {
        $tableGateway = $this->getUserTableGateway();
        $mapper = new \Manager\Model\User\UserMapper($tableGateway);
        $mapper->setServiceLocator($this->serviceLocator);
        return $mapper;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}