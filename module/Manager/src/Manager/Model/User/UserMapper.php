<?php

namespace Manager\Model\User;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

class UserMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getUserModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }


    /**
     *
     * @return resultset
     */
    public function getUserModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->order('id DESC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return UserModel
     */
    public function getUserModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param UserModel $model
     */
    public function saveUserModel(UserModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param UserModel|int $model
     */
    public function deleteUserModel($model)
    {
        if ($model instanceof UserModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $status
     * @return UserModel
     */
    public function getUserModelsByStatus($status)
    {
        return $this->tableGateway->select(array('status' => $status));
    }


    /**
     *
     * @param mixed $status
     * @return ResultSet
     */
    public function getUserModelSetByStatus($status)
    {
        return $this->tableGateway->select(array('status' => $status));
    }

    /**
     *
     * @param mixed $id
     * @return UserModel
     */
    public function getUserModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getUserModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getUserModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getUserModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}