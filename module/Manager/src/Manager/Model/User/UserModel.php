<?php

namespace Manager\Model\User;

class UserModel
{

    protected $data = array(
        'name' => '',
        'email' => '',
        'password' => '',
        'status' => '1',
        'date_create' => '',
        'id' => NULL,
    );

    public function getName()
    {
        return $this->data['name'];
    }

    public function setName($name)
    {
        $this->data['name'] = $name;
    }
    public function getEmail()
    {
        return $this->data['email'];
    }

    public function setEmail($email)
    {
        $this->data['email'] = $email;
    }
    public function getPassword()
    {
        return $this->data['password'];
    }

    public function setPassword($password)
    {
        $this->data['password'] = $password;
    }
    public function getStatus()
    {
        return $this->data['status'];
    }

    public function setStatus($status)
    {
        $this->data['status'] = $status;
    }
    public function getDateCreate()
    {
        return $this->data['date_create'];
    }

    public function setDateCreate($dateCreate)
    {
        $this->data['date_create'] = $dateCreate;
    }
    public function getId()
    {
        return $this->data['id'];
    }

    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    public function exchangeArray($data)
    {
        foreach ($data as $key => $value)
        {
            if (!array_key_exists($key, $this->data)) {
                continue;//throw new \Exception("$key field does not exist in " . __CLASS__);
            }
            if ($key == "dateCreate") {
                $this->data[$key] = date("Y-m-d H:i:s");
            } else {
                $this->data[$key] = $value;
            }
        }
    }

    public function toArray()
    {
        return $this->data;
    }
    
    public function getArrayCopy()
    {
        return $this->data;
    }
    
    public function getMyName()
    {
        return strtolower("User");
    }
}