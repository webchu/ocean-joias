<?php

namespace Manager\Model\Place;

class PlaceModel
{

    protected $data = array(
        'name' => '',
        'address' => '',
        'number' => '',
        'address_notes' => '',
        'city' => '',
        'state' => '',
        'postalcode' => '',
        'phone' => '',
        'cellphone' => '',
        'phone_others' => '',
        'whatsapp' => '',
        'email' => '',
        'website' => '',
        'operating_hours' => '',
        'id' => NULL,
    );

    public function getName()
    {
        return $this->data['name'];
    }

    public function setName($name)
    {
        $this->data['name'] = $name;
    }
    public function getAddress()
    {
        return $this->data['address'];
    }

    public function setAddress($address)
    {
        $this->data['address'] = $address;
    }
    public function getNumber()
    {
        return $this->data['number'];
    }

    public function setNumber($number)
    {
        $this->data['number'] = $number;
    }
    public function getAddressNotes()
    {
        return $this->data['address_notes'];
    }

    public function setAddressNotes($addressNotes)
    {
        $this->data['address_notes'] = $addressNotes;
    }
    public function getCity()
    {
        return $this->data['city'];
    }

    public function setCity($city)
    {
        $this->data['city'] = $city;
    }
    public function getState()
    {
        return $this->data['state'];
    }

    public function setState($state)
    {
        $this->data['state'] = $state;
    }
    public function getPostalcode()
    {
        return $this->data['postalcode'];
    }

    public function setPostalcode($postalcode)
    {
        $this->data['postalcode'] = $postalcode;
    }
    public function getPhone()
    {
        return $this->data['phone'];
    }

    public function setPhone($phone)
    {
        $this->data['phone'] = $phone;
    }
    public function getCellphone()
    {
        return $this->data['cellphone'];
    }

    public function setCellphone($cellphone)
    {
        $this->data['cellphone'] = $cellphone;
    }
    public function getPhoneOthers()
    {
        return $this->data['phone_others'];
    }

    public function setPhoneOthers($phoneOthers)
    {
        $this->data['phone_others'] = $phoneOthers;
    }
    public function getWhatsapp()
    {
        return $this->data['whatsapp'];
    }

    public function setWhatsapp($whatsapp)
    {
        $this->data['whatsapp'] = $whatsapp;
    }
    public function getEmail()
    {
        return $this->data['email'];
    }

    public function setEmail($email)
    {
        $this->data['email'] = $email;
    }
    public function getWebsite()
    {
        return $this->data['website'];
    }

    public function setWebsite($website)
    {
        $this->data['website'] = $website;
    }
    public function getOperatingHours()
    {
        return $this->data['operating_hours'];
    }

    public function setOperatingHours($operatingHours)
    {
        $this->data['operating_hours'] = $operatingHours;
    }
    public function getId()
    {
        return $this->data['id'];
    }

    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    public function exchangeArray($data)
    {
        foreach ($data as $key => $value)
        {
            if (!array_key_exists($key, $this->data)) {
                continue;//throw new \Exception("$key field does not exist in " . __CLASS__);
            }
            if ($key == "dateCreate") {
                $this->data[$key] = date("Y-m-d H:i:s");
            } else {
                $this->data[$key] = $value;
            }
        }
    }

    public function toArray()
    {
        return $this->data;
    }
    
    public function getArrayCopy()
    {
        return $this->data;
    }
    
    public function getMyName()
    {
        return strtolower("Place");
    }
}