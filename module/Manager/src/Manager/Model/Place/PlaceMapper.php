<?php

namespace Manager\Model\Place;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

use Manager\Model\ModelFactory;

class PlaceMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getPlaceModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }
    
    private function _getProperties($id)
    {
        $properties = array();
        $mPlaceProperty = ModelFactory::getInstance()->getPlacePropertyMapper();
        $properties = $mPlaceProperty->getPlacePropertyModelByCriteria(array("id_place" => $id));
        return $properties->toArray();
    }
    
    private function _mountProperties($properties)
    {
        $tempProperties = array();
        if (!empty($properties)) :
            foreach($properties as $property) :
                $tempProperties[$property["key"]] = $property["value"];
            endforeach;
        endif;
        return $tempProperties;
    }
    
    public function getPlacesWithProperties()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->order('id DESC');
        });
        $places = array();
        foreach($rowset as $row) :
            $place = $row->toArray();
            //$place['properties'] = $this->_mountProperties($this->_getProperties($row->getId()));
            $place['properties'] = $this->_getProperties($row->getId());
            $places[] = $place;
        endforeach;
        return $places;
    }



    /**
     *
     * @return resultset
     */
    public function getPlaceModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->order('id DESC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return PlaceModel
     */
    public function getPlaceModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param PlaceModel $model
     */
    public function savePlaceModel(PlaceModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param PlaceModel|int $model
     */
    public function deletePlaceModel($model)
    {
        if ($model instanceof PlaceModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return PlaceModel
     */
    public function getPlaceModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getPlaceModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getPlaceModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getPlaceModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}