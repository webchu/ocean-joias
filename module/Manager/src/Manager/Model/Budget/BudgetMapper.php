<?php

namespace Manager\Model\Budget;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

use Manager\Model\ModelFactory;

class BudgetMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getBudgets($ids)
    {
        $sql = "SELECT *
        FROM
            budget
        WHERE
            id IN (" . $ids . ")";
        
        //$resultSet = $this->tableGateway->getAdapter()->query($sql, array(5));
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }

    public function getBudgetItems($budget_id)
    {
        $sql = "SELECT 
            p.id, p.name, p.style, p.reference, p.price, c.id as category_id, c.name as category,
            bi.quantity
        FROM
            product p
            JOIN product_category pca ON pca.product_id = p.id
            JOIN category c ON c.id = pca.category_id
            JOIN budget_items bi ON bi.product_id = p.id
        WHERE
            bi.budget_id = " . (int) $budget_id;
        
        //$resultSet = $this->tableGateway->getAdapter()->query($sql, array(5));
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }

    public function getBudgetModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }


    /**
     *
     * @return resultset
     */
    public function getBudgetModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->order('id DESC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return BudgetModel
     */
    public function getBudgetModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param BudgetModel $model
     */
    public function saveBudgetModel(BudgetModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param BudgetModel|int $model
     */
    public function deleteBudgetModel($model)
    {
        if ($model instanceof BudgetModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return BudgetModel
     */
    public function getBudgetModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getBudgetModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getBudgetModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getBudgetModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}