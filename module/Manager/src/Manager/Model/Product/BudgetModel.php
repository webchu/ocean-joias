<?php

namespace Manager\Model\Product;

class BudgetModel
{

    protected $data = array(
        'email' => '',
        'name' => '',
        'phone' => '',
        'message' => '',
        'date' => NULL,
        'id' => NULL,
    );

    public function getEmail() { return $this->data['email']; }
    public function setEmail($email) { $this->data['email'] = $email; }

    public function getName() { return $this->data['name']; }
    public function setName($name) { $this->data['name'] = $name; }

    public function getPhone() { return $this->data['phone']; }
    public function setPhone($phone) { $this->data['phone'] = $phone; }

    public function getMessage() { return $this->data['message']; }
    public function setMessage($message) { $this->data['message'] = $message; }

    public function getDate() { return $this->data['date']; }
    public function setDate($date) { $this->data['date'] = $date; }
    
    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    public function exchangeArray($data)
    {
        foreach ($data as $key => $value)
        {
            if (!array_key_exists($key, $this->data)) {
                continue;//throw new \Exception("$key field does not exist in " . __CLASS__);
            }
            if ($key == "dateCreate") {
                $this->data[$key] = date("Y-m-d H:i:s");
            } else {
                $this->data[$key] = $value;
            }
        }
    }

    public function toArray()
    {
        return $this->data;
    }
    
    public function getArrayCopy()
    {
        return $this->data;
    }
    
    public function getMyName()
    {
        return strtolower("Budget");
    }
}