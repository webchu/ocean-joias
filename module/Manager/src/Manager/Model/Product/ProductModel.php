<?php

namespace Manager\Model\Product;

class ProductModel
{

    protected $data = array(
        'reference' => '',
        'name' => '',
        'description' => '',
        'photo' => '',
        'price' => 0,
        'id' => NULL,
    );

    public function getPrice()
    {
        return $this->data['price'];
    }

    public function setPrice($price)
    {
        $this->data['price'] = $price;
    }

    public function getReference()
    {
        return $this->data['reference'];
    }

    public function setReference($reference)
    {
        $this->data['reference'] = $reference;
    }
    
    public function getName()
    {
        return $this->data['name'];
    }

    public function setName($name)
    {
        $this->data['name'] = $name;
    }
    public function getDescription()
    {
        return $this->data['description'];
    }

    public function setDescription($description)
    {
        $this->data['description'] = $description;
    }
    public function getPhoto()
    {
        return $this->data['photo'];
    }

    public function setPhoto($photo)
    {
        $this->data['photo'] = $photo;
    }
    public function getId()
    {
        return $this->data['id'];
    }

    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    public function exchangeArray($data)
    {
        foreach ($data as $key => $value)
        {
            if (!array_key_exists($key, $this->data)) {
                continue;//throw new \Exception("$key field does not exist in " . __CLASS__);
            }
            if ($key == "dateCreate") {
                $this->data[$key] = date("Y-m-d H:i:s");
            } else {
                $this->data[$key] = $value;
            }
        }
    }

    public function toArray()
    {
        return $this->data;
    }
    
    public function getArrayCopy()
    {
        return $this->data;
    }
    
    public function getMyName()
    {
        return strtolower("Product");
    }
}