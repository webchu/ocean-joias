<?php

namespace Manager\Model\Product;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

use Manager\Model\ModelFactory;

class ProductMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getProducts($ids)
    {
        $sql = "SELECT *
        FROM
            product
        WHERE
            id IN (" . $ids . ")";
        
        //$resultSet = $this->tableGateway->getAdapter()->query($sql, array(5));
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }

    public function searchProducts($name)
    {
        $sql = "select 
            p.id, p.name, p.price, co.name as colecao, ca.name as categoria
        from 
            product p
            left join product_collection pc
                on pc.product_id = p.id
            left join collection co
                on co.id = pc.collection_id

            left join product_category pca
                on pca.product_id = p.id
            left join category ca
                on ca.id = pca.category_id
        where 
            p.name like '%" . $name . "%'";
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }

    public function getProdutosRandom($limit = 6)
    {
        $sql = "SELECT 
            p.id, p.name, p.style, p.reference, c.id as category_id, c.name as category
        FROM
            product p
            JOIN product_category pca ON pca.product_id = p.id
            JOIN category c ON c.id = pca.category_id
        WHERE
            1=1
        ORDER BY RAND()
        LIMIT " . $limit;
        
        //$resultSet = $this->tableGateway->getAdapter()->query($sql, array(5));
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }

    public function getProductModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }


    /**
     *
     * @return resultset
     */
    public function getProductModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
             $select->order('id DESC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return ProductModel
     */
    public function getProductModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param ProductModel $model
     */
    public function saveProductModel(ProductModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param ProductModel|int $model
     */
    public function deleteProductModel($model)
    {
        if ($model instanceof ProductModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ProductModel
     */
    public function getProductModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getProductModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getProductModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getProductModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}