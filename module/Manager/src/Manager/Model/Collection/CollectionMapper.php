<?php

namespace Manager\Model\Collection;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

use Manager\Model\ModelFactory;

class CollectionMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getColecoes()
    {
        $sql = "select pc.collection_id, pc.product_id, c.name collection_name, p.name product_name
from product_collection pc
join collection c on c.id = pc.collection_id
join product p on p.id = pc.product_id
group by pc.collection_id
order by c.id DESC";
        
        //$resultSet = $this->tableGateway->getAdapter()->query($sql, array(5));
        $resultSet = $this->tableGateway->getAdapter()->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        return $resultSet->toArray();
    }

    public function getCollectionModelByCriteria(array $criteria)
    {
        return $this->tableGateway->select($criteria);
    }


    /**
     *
     * @return resultset
     */
    public function getCollectionModels()
    {
        $tableGateway = $this->tableGateway;
        $rowset = $tableGateway->select(function (Select $select) {
            $select->where('status = 1');
            $select->order('id DESC');
        });
        return $rowset;
    }

    /**
     * @param int $id
     * @return CollectionModel
     */
    public function getCollectionModel($id)
    {
        return $this->tableGateway->select(array('id' => $id))->current();
    }

    /**
     * @param CollectionModel $model
     */
    public function saveCollectionModel(CollectionModel &$model)
    {
        $id = $model->getId();

        if (!$id) {
            $this->tableGateway->insert($model->toArray());
            $model->setId($this->tableGateway->lastInsertValue);
            return $model->getId();
        } else {
            $this->tableGateway->update($model->toArray(), array('id' => $id));
        }
    }

    /**
     *
     * @param CollectionModel|int $model
     */
    public function deleteCollectionModel($model)
    {
        if ($model instanceof CollectionModel) {
            $id = $model->getId();
        } else {
            $id = $model;
        }

        $this->tableGateway->delete(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return CollectionModel
     */
    public function getCollectionModelsById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }


    /**
     *
     * @param mixed $id
     * @return ResultSet
     */
    public function getCollectionModelSetById($id)
    {
        return $this->tableGateway->select(array('id' => $id));
    }

    public function getCollectionModelsAsArray() {
        $arrayResult = array();
        $rowset = $this->getCollectionModels();
        foreach($rowset as $row) {
            $arrayResult[$row->getId()] = $row->toArray();
        }
        return $arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}