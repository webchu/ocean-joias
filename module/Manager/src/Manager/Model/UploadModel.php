<?php

namespace Manager\Model;

use Aws\S3\S3Client;

class UploadModel
{
    private $_finalPath = null;
    
    private $_finalName = null;
    
    /**
     * 
     * @param: $files
     * @param: $targetFolder    If is empty, send to /images
     * 
     */
    public function receive($file, $prefix = "", $targetFolder = "images") {
        $destination = realpath(dirname(__DIR__ . "/../../../../../../"));
        $originalFilename = basename($file['name']);
        $newFilename = $this->_finalName = $prefix . '-' . uniqid() . '-' . $originalFilename;
        $target = $destination . "/public/" . $targetFolder . "/" . $newFilename;
        //echo "File <pre>" . print_r($file['tmp_name'], 1) . "</pre>";
        //echo "Prefix <pre>" . print_r($target, 1) . "</pre>";
        //exit;
        
        move_uploaded_file($file['tmp_name'], $target);
        $this->_finalPath = $target;
        return $this->_finalName;
        
        // $adapter = new \Zend\File\Transfer\Adapter\Http(); 
        // $adapter->setDestination($destination . "/public/".$targetFolder."/.");
        // $this->_finalPath = $destination . "/public/".$targetFolder."/".$file['name'];
        // $adapter->addFilter('Rename', $newFilename);
        
        // if ($adapter->receive()) {
        //     //return true;
        //     $this->_finalPath = $adapter->getFileName();
        //     return $this->_finalPath;
        // } else {
        //     $messages = $adapter->getMessages();
        //     throw new \Exception("Error on Upload. -> " . implode("\n", $messages));
        // }
    }
    
    public function sendToS3($file, $prefix = "", $targetFolder = "stuffs") {
        //echo "<pre>" . print_r(func_get_args()) . "</pre>";
        //exit;
        if (empty($file['name']) || empty($file['tmp_name'])) {
            throw new \Exception("Invalid File!");
        }
        $this->_finalName = empty($prefix) ? uniqid() . '-' . basename($file['name']) : $prefix . '-' . uniqid() . '-' . basename($file['name']);
        $this->_saveS3($file['tmp_name'], $targetFolder, $this->_finalName);
        return $this->_finalName;
    }
    
    private function _saveS3($pathSource, $path, $targetName)
    {
        // Instantiate an Amazon S3 client.
        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'us-west-2',
            'credentials' => array(
                'key' => 'AKIAIFSPRXCVSYBRLCIA',
                'secret'  => '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',
            )
        ]);

        $s3->putObject([
            'Bucket' => 'hdmotos-images',
            'Key'    => $path . '/' . $targetName,
            'Body'   => fopen($pathSource, 'r'),
            'ACL'    => 'public-read',
        ]);
        $this->_finalPath = 'hdmotos-images' . "/" . $path . '/' . $targetName;
    }
    
    public function getPath()
    {
        return $this->_finalPath;
    }
}