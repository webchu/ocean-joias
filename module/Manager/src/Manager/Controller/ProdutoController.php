<?php

namespace Manager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\Produto\ProdutoModel;

class ProdutoController extends AbstractActionController
{

    protected $_produto = null;
    
    public function __construct()
    {
        $this->_produto = ModelFactory::getInstance()->getProdutoMapper();
    }
    
    public function indexAction()
    {
        
        return new ViewModel(array(
            "elements" => $this->_produto->getProdutoModels(),
            "entity" => "produto",
            
        ));
    }
    
    public function createAction()
    {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new ProdutoModel();
            $model->exchangeArray($request->getPost());
            $this->_produto->saveProdutoModel($model);
            return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "produto"));
        } else {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!is_null($id) && $id > 0) {
                $model = $this->_produto->getProdutoModel($id);
                if (!is_object($model)) {
                    throw new \Exception("Not found Model");
                }
            } else {
                $model = new ProdutoModel();
            }
        }
        return new ViewModel(array(
            "model" => $model,
            
        ));
    }
    
    public function removeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!is_null($id) && $id > 0) {
            $model = $this->_produto->getProdutoModel($id);
            if (is_object($model)) {
                $this->_produto->deleteProdutoModel($model);
            } else throw new \Exception("Produto Not Found", 1);
        }
        return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "produto"));
    }
}