<?php

namespace Manager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\User\UserModel;

class UserController extends AbstractActionController
{

    protected $_user = null;
    
    public function __construct()
    {
        $this->_user = ModelFactory::getInstance()->getUserMapper();
    }
    
    public function indexAction()
    {
        
        return new ViewModel(array(
            "elements" => $this->_user->getUserModels(),
            "entity" => "user",
            
        ));
    }
    
    public function createAction()
    {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new UserModel();
            $model->exchangeArray($request->getPost());
            $this->_user->saveUserModel($model);
            return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "user"));
        } else {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!is_null($id) && $id > 0) {
                $model = $this->_user->getUserModel($id);
                if (!is_object($model)) {
                    throw new \Exception("Not found Model");
                }
            } else {
                $model = new UserModel();
            }
        }
        return new ViewModel(array(
            "model" => $model,
            
        ));
    }
    
    public function removeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!is_null($id) && $id > 0) {
            $model = $this->_user->getUserModel($id);
            if (is_object($model)) {
                $this->_user->deleteUserModel($model);
            } else throw new \Exception("User Not Found", 1);
        }
        return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "user"));
    }
}