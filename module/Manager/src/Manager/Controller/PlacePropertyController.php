<?php

namespace Manager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\PlaceProperty\PlacePropertyModel;

class PlacePropertyController extends AbstractActionController
{

    protected $_placeproperty = null;
    
    public function __construct()
    {
        $this->_placeproperty = ModelFactory::getInstance()->getPlacePropertyMapper();
    }
    
    public function indexAction()
    {
        $mapperPlace = ModelFactory::getInstance()->getPlaceMapper();

        return new ViewModel(array(
            "elements" => $this->_placeproperty->getPlacePropertyModels(),
            "entity" => "placeproperty",
            "places" => $mapperPlace->getPlaceModelsAsArray(),

        ));
    }
    
    public function createAction()
    {
        $mapperPlace = ModelFactory::getInstance()->getPlaceMapper();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new PlacePropertyModel();
            $model->exchangeArray($request->getPost());
            $this->_placeproperty->savePlacePropertyModel($model);
            return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "placeproperty"));
        } else {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!is_null($id) && $id > 0) {
                $model = $this->_placeproperty->getPlacePropertyModel($id);
                if (!is_object($model)) {
                    throw new \Exception("Not found Model");
                }
            } else {
                $model = new PlacePropertyModel();
            }
        }
        return new ViewModel(array(
            "model" => $model,
            "places" => $mapperPlace->getPlaceModelsAsArray(),

        ));
    }
    
    public function removeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!is_null($id) && $id > 0) {
            $model = $this->_placeproperty->getPlacePropertyModel($id);
            if (is_object($model)) {
                $this->_placeproperty->deletePlacePropertyModel($model);
            } else throw new \Exception("PlaceProperty Not Found", 1);
        }
        return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "placeproperty"));
    }
}