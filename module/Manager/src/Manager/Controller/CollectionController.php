<?php

namespace Manager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\Collection\CollectionModel;

class CollectionController extends AbstractActionController
{

    protected $_collection = null;
    
    public function __construct()
    {
        $this->_collection = ModelFactory::getInstance()->getCollectionMapper();
    }
    
    public function indexAction()
    {
        
        return new ViewModel(array(
            "elements" => $this->_collection->getCollectionModels(),
            "entity" => "collection",
            
        ));
    }
    
    public function createAction()
    {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new CollectionModel();
            $model->exchangeArray($request->getPost());
            $this->_collection->saveCollectionModel($model);
            return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "collection"));
        } else {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!is_null($id) && $id > 0) {
                $model = $this->_collection->getCollectionModel($id);
                if (!is_object($model)) {
                    throw new \Exception("Not found Model");
                }
            } else {
                $model = new CollectionModel();
            }
        }
        return new ViewModel(array(
            "model" => $model,
            
        ));
    }
    
    public function removeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!is_null($id) && $id > 0) {
            $model = $this->_collection->getCollectionModel($id);
            if (is_object($model)) {
                $this->_collection->deleteCollectionModel($model);
            } else throw new \Exception("Collection Not Found", 1);
        }
        return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "collection"));
    }
}