<?php

namespace Manager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\Product\ProductModel;

class BudgetController extends AbstractActionController
{

    protected $_budget = null;
    
    public function __construct()
    {
        $this->_budget = ModelFactory::getInstance()->getBudgetMapper();
    }
    
    public function indexAction()
    {
        
        return new ViewModel(array(
            "elements" => $this->_budget->getBudgetModels(),
            "entity" => "budget",
            
        ));
    }

    public function detailAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        $budget = $this->_budget->getBudgetModel($id);

        $items = $this->_budget->getBudgetItems($id);
        return new ViewModel(array(
            'id' => $id,
            'budget' => $budget->toArray(),
            'items' => $items
        ));
    }
    
    public function createAction()
    {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new ProductModel();
            $model->exchangeArray($request->getPost());
            $this->_product->saveProductModel($model);
            return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "product"));
        } else {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!is_null($id) && $id > 0) {
                $model = $this->_product->getProductModel($id);
                if (!is_object($model)) {
                    throw new \Exception("Not found Model");
                }
            } else {
                $model = new ProductModel();
            }
        }
        return new ViewModel(array(
            "model" => $model,
            
        ));
    }
    
    public function removeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!is_null($id) && $id > 0) {
            $model = $this->_budget->getBudgetModel($id);
            if (is_object($model)) {
                $this->_budget->deleteBudgetModel($model);
            } else throw new \Exception("Budget Not Found", 1);
        }
        return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "budget"));
    }
}