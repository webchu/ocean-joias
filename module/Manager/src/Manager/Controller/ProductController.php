<?php

namespace Manager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\Product\ProductModel;

class ProductController extends AbstractActionController
{

    protected $_product = null;
    
    public function __construct()
    {
        $this->_product = ModelFactory::getInstance()->getProductMapper();
    }
    
    public function indexAction()
    {
        
        return new ViewModel(array(
            "elements" => $this->_product->getProductModels(),
            "entity" => "product",
            
        ));
    }
    
    public function createAction()
    {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new ProductModel();
            $model->exchangeArray($request->getPost());
            $this->_product->saveProductModel($model);
            return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "product"));
        } else {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!is_null($id) && $id > 0) {
                $model = $this->_product->getProductModel($id);
                if (!is_object($model)) {
                    throw new \Exception("Not found Model");
                }
            } else {
                $model = new ProductModel();
            }
        }
        return new ViewModel(array(
            "model" => $model,
            
        ));
    }
    
    public function removeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!is_null($id) && $id > 0) {
            $model = $this->_product->getProductModel($id);
            if (is_object($model)) {
                $this->_product->deleteProductModel($model);
            } else throw new \Exception("Product Not Found", 1);
        }
        return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "product"));
    }
}