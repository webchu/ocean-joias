<?php

namespace Manager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\ProductCategory\ProductCategoryModel;

class ProductCategoryController extends AbstractActionController
{

    protected $_productcategory = null;
    
    public function __construct()
    {
        $this->_productcategory = ModelFactory::getInstance()->getProductCategoryMapper();
    }
    
    public function indexAction()
    {
        
        return new ViewModel(array(
            "elements" => $this->_productcategory->getProductCategoryModels(),
            "entity" => "productcategory",
            
        ));
    }
    
    public function createAction()
    {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new ProductCategoryModel();
            $model->exchangeArray($request->getPost());
            $this->_productcategory->saveProductCategoryModel($model);
            return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "productcategory"));
        } else {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!is_null($id) && $id > 0) {
                $model = $this->_productcategory->getProductCategoryModel($id);
                if (!is_object($model)) {
                    throw new \Exception("Not found Model");
                }
            } else {
                $model = new ProductCategoryModel();
            }
        }
        return new ViewModel(array(
            "model" => $model,
            
        ));
    }
    
    public function removeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!is_null($id) && $id > 0) {
            $model = $this->_productcategory->getProductCategoryModel($id);
            if (is_object($model)) {
                $this->_productcategory->deleteProductCategoryModel($model);
            } else throw new \Exception("ProductCategory Not Found", 1);
        }
        return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "productcategory"));
    }
}