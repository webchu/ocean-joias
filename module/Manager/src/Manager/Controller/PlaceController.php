<?php

namespace Manager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\Place\PlaceModel;

class PlaceController extends AbstractActionController
{

    protected $_place = null;
    
    public function __construct()
    {
        $this->_place = ModelFactory::getInstance()->getPlaceMapper();
    }
    
    public function indexAction()
    {
        
        return new ViewModel(array(
            "elements" => $this->_place->getPlaceModels(),
            "entity" => "place",
            
        ));
    }
    
    public function createAction()
    {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new PlaceModel();
            $model->exchangeArray($request->getPost());
            $this->_place->savePlaceModel($model);
            return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "place"));
        } else {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!is_null($id) && $id > 0) {
                $model = $this->_place->getPlaceModel($id);
                if (!is_object($model)) {
                    throw new \Exception("Not found Model");
                }
            } else {
                $model = new PlaceModel();
            }
        }
        return new ViewModel(array(
            "model" => $model,
            
        ));
    }
    
    public function removeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!is_null($id) && $id > 0) {
            $model = $this->_place->getPlaceModel($id);
            if (is_object($model)) {
                $this->_place->deletePlaceModel($model);
            } else throw new \Exception("Place Not Found", 1);
        }
        return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "place"));
    }
}