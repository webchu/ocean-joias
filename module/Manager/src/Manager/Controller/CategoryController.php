<?php

namespace Manager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\Category\CategoryModel;

class CategoryController extends AbstractActionController
{

    protected $_category = null;
    
    public function __construct()
    {
        $this->_category = ModelFactory::getInstance()->getCategoryMapper();
    }
    
    public function indexAction()
    {
        
        return new ViewModel(array(
            "elements" => $this->_category->getCategoryModels(),
            "entity" => "category",
            
        ));
    }
    
    public function createAction()
    {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new CategoryModel();
            $model->exchangeArray($request->getPost());
            $this->_category->saveCategoryModel($model);
            return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "category"));
        } else {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!is_null($id) && $id > 0) {
                $model = $this->_category->getCategoryModel($id);
                if (!is_object($model)) {
                    throw new \Exception("Not found Model");
                }
            } else {
                $model = new CategoryModel();
            }
        }
        return new ViewModel(array(
            "model" => $model,
            
        ));
    }
    
    public function removeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!is_null($id) && $id > 0) {
            $model = $this->_category->getCategoryModel($id);
            if (is_object($model)) {
                $this->_category->deleteCategoryModel($model);
            } else throw new \Exception("Category Not Found", 1);
        }
        return $this->redirect()->toRoute(strtolower('Manager'), array("controller" => "category"));
    }
}