<?php
return array(
    "Skeleton Application" => 'Aplicação Padrão',
    "Home" => "Ínicio",
    "Community" => "Comunidades",
    "DateCreate" => "Criado em",
    "Research"  => "Pesquisas",
    "DateStart"  => "Inicio",
    "DateEnd"  => "Termina",
    "Member"  => "Membro",
    "Required"  => "Obrigatório",
    //Menu
    "Category"  => "Categoria",
    "Partner"   => "Parceiro",
    "Address"   => "Endereço",
    "Product"   => "Produto",
    "Setting"   => "Configuração",
    "User"      => "Usuário",
    //General
    "Add"       => "Adicionar",
    "Name"      => "Nome",
    "Actions"   => "Ações",
    "edit"      => "Editar",
    "Edit"      => "Editar",
    "delete"    => "Remover",
    "Description"   => "Descrição",
    "Sign In"   => "Autenticar-se",
    "Signed in as"  => "Logado como: ",
    //Partner
    "Category_id"   => "Categoria",
    "Phone"     => "Telefone",
    "Order"     => "Ordem",
    "Notes"     => "Observações",
    //Address
    "Number"    => "Número",
    "Address_notes" => "Complemento",
    "Neighbourhood" => "Bairro",
    "City"      => "Cidade",
    "State"     => "Estado",
    "Zipcode"   => "Cep",
    "Partner_id"    => "Parceiro",
    "partner_id"    => "Parceiro",
    //Product
    "Discount"  => "Desconto",
    "Discount_type"  => "Tipo de Desconto",
    "discount_type"  => "Tipo de Desconto",
    //Setting
    "Key"       => "Chave",
    "Value"     => "Valor",
    //user
    "Password"  => "Senha",
    "Display_name"  => "Nome",
    "User_id"   => "Id",
    "Username"  => "Usuário",
    "Article" => "Artigo",
    "Files" => "Arquivos",
    "File" => "Arquivo",
    "Section" => "Seção",
    "Title" => "Título",
    "Text" => "Texto",
    "Sumary" => "Resumo",
    "Section_id" => "Seção",
    "Subtitle" => "Subtítulo",
    "Faq" => "Perguntas frequentes",
    "News" => "Notícias",
    "Lead" => "Resumo",
    "Question" => "Pergunta",
    "Answer" => "Resposta",
    "Entity" => "Entidade",
    "Filename" => "Nome do arquivo"
);
?>