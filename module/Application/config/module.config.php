<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'collection' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/colecao',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Collection',
                        'action'     => 'index',
                    ),
                    'constraints' => [
                         'id'            => '[0-9]+',
                     ]
                ),
            ),
            'colecoes' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/colecoes',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Collection',
                        'action'     => 'todas',
                    ),
                ),
            ),
            'categoria' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/categoria',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Category',
                        'action'     => 'index',
                    ),
                ),
            ),
            'quemSomos' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/quem-somos',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'quemSomos',
                    ),
                ),
            ),
            'faleConosco' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/fale-conosco',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'faleConosco',
                    ),
                ),
            ),
            'entrega' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/entrega',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'entrega',
                    ),
                ),
            ),
            'faq' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/faq',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'faq',
                    ),
                ),
            ),
            'notfound' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/404',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'notfound',
                    ),
                ),
            ),
            'product' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/orcamento',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Product',
                        'action'     => 'quote',
                    ),
                ),
            ),
            'categorizar' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/categorizar',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Product',
                        'action'     => 'categorizar',
                    ),
                ),
            ),
            'orcar' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/orcar',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Budget',
                        'action'     => 'orcar',
                    ),
                ),
            ),
            'pesquisar' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/pesquisar',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Product',
                        'action'     => 'pesquisar',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => Controller\IndexController::class,
            'Application\Controller\Register' => Controller\RegisterController::class,
            'Application\Controller\Authenticate' => Controller\AuthenticateController::class,
            'Application\Controller\Search' => Controller\SearchController::class,
            'Application\Controller\Product' => Controller\ProductController::class,
            'Application\Controller\Panel' => Controller\PanelController::class,
            'Application\Controller\Advertise' => Controller\AdvertiseController::class,
            'Application\Controller\Checkout' => Controller\CheckoutController::class,
            'Application\Controller\Content' => Controller\ContentController::class,
            'Application\Controller\Contact' => Controller\ContactController::class,
            'Application\Controller\Collection' => Controller\CollectionController::class,
            'Application\Controller\Category' => Controller\CategoryController::class,
            'Application\Controller\Budget' => Controller\BudgetController::class,
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'familiesAndModels'  => 'Application\View\Helper\FamiliesAndModels',
            'hPlate'  => 'Application\View\Helper\Plate',
            'hCurrency' => 'Application\View\Helper\Currency',
            'hDictionary' => 'Application\View\Helper\Dictionary',
            'productLink' => 'Application\View\Helper\ProductLink',
      ),
   ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
