<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class ProductLink extends AbstractHelper
{
    public function __invoke($item)
    {
        $model = str_replace(" ", "-", $item['model']);
        $link = "/comprar/" . $model . "-" . $item['properties']['year_fab'] . "-" . 
            $item['properties']['year_model'] . "/" . $item['id'];
        return $link;
    }
}