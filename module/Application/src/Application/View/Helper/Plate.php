<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Plate extends AbstractHelper
{
    public function __invoke($plate)
    {
        $first = substr($plate, 0, 1);
        $last  = substr($plate, -1);
        return sprintf('%sXX-XX%s', $first, $last);
    }
}