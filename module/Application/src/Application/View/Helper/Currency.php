<?php

namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
 
class Currency extends AbstractHelper
{
    public function __invoke($value = 0)
    {
        return 'R$ ' . number_format($value, 2, ',', '.');
    }
}