<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Manager\Model\ModelFactory;

class FamiliesAndModels extends AbstractHelper
{
    public function __invoke($families, $models)
    {
        $list = [];
        foreach($families as $cont => $family) :
        	$_models = [];
        	foreach($models as $cont2 => $model) :
        		if ($model['id_family'] == $family['id']) :
        			$_models[] = $model;
        		endif;
        	endforeach;
        	$list[] = array(
        		'family' => $family,
        		'models' => $_models
        	);
        endforeach;
        
        return $list;
    }
}