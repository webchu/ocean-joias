<?php

namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;

class Dictionary extends AbstractHelper
{
    public function __invoke($term)
    {
        $terms = array(
        	'accept' 		=> 'Aceita troca',
        	'cc' 			=> 'Cilindradas',
        	'city' 			=> 'Cidade',
        	'color_primary' => 'Cor predominante',
        	'color_secondary' => 'Cor secundária',
        	'custom' 		=> 'Moto customizada',
        	'cv' 			=> 'Potência',
        	'financing' 	=> 'Moto financiada',
        	'ipva' 			=> 'IPVA do ano atual pago',
        	'km' 			=> 'KM',
        	'last_revision' => 'Data da última revisão',
        	'plate' 		=> 'Placa',
        	'rushmore' 		=> 'Projeto Rushmore',
        	'state' 		=> 'Estado',
        	'video'			=> 'Vídeo',
        	'year_fab' 		=> 'Ano de fabricação',
        	'year_model' 	=> 'Ano do modelo',
        	'acessory'      => 'Acessórios',
        );

        return ($terms[$term] != '') ? $terms[$term] : $term;
    }
}