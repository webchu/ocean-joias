<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Manager\Model\ModelFactory;

class IndexController extends AbstractActionController
{
    public function __construct() {
    }
    
    public function indexAction()
    {
        $mCategory   = ModelFactory::getInstance()->getCategoryMapper();
        $mProduct    = ModelFactory::getInstance()->getProductMapper();
        $mCollection = ModelFactory::getInstance()->getCollectionMapper();
        
        return new ViewModel(array(
            //'places' => $mPlace->getPlaceModels()->toArray()
            'categories' => $mCategory->getCategoryModels()->toArray(),
            'products' => $mProduct->getProdutosRandom(9),
            'collections' => $mCollection->getCollectionModels()->toArray(),
        ));
    }

    public function quemSomosAction()
    {
        $mCategory   = ModelFactory::getInstance()->getCategoryMapper();
        $mCollection = ModelFactory::getInstance()->getCollectionMapper();
        
        return new ViewModel(array(
            'categories' => $mCategory->getCategoryModels()->toArray(),
            'collections' => $mCollection->getCollectionModels()->toArray(),
        ));
    }

    public function faleConoscoAction()
    {
        $mCategory   = ModelFactory::getInstance()->getCategoryMapper();
        $mCollection = ModelFactory::getInstance()->getCollectionMapper();
        
        return new ViewModel(array(
            'categories' => $mCategory->getCategoryModels()->toArray(),
            'collections' => $mCollection->getCollectionModels()->toArray(),
        ));
    }

    public function entregaAction()
    {
        $mCategory   = ModelFactory::getInstance()->getCategoryMapper();
        $mCollection = ModelFactory::getInstance()->getCollectionMapper();
        
        return new ViewModel(array(
            'categories' => $mCategory->getCategoryModels()->toArray(),
            'collections' => $mCollection->getCollectionModels()->toArray(),
        ));
    }

    public function faqAction()
    {
        $mCategory   = ModelFactory::getInstance()->getCategoryMapper();
        $mCollection = ModelFactory::getInstance()->getCollectionMapper();
        
        return new ViewModel(array(
            'categories' => $mCategory->getCategoryModels()->toArray(),
            'collections' => $mCollection->getCollectionModels()->toArray(),
        ));
    }

    public function notfoundAction()
    {
    	return new ViewModel();
    }
}
