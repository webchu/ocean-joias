<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Manager\Model\ModelFactory;
use Manager\Model\ProductCategory\ProductCategoryModel;
use Zend\View\Model\JsonModel;

class ProductController extends AbstractActionController
{
    public function __construct() {
    }
    
    public function quoteAction()
    {
        $request = $this->getRequest();
        $ids = $request->getQuery('products');
        
        $mProduct = ModelFactory::getInstance()->getProductMapper();
        $products = $mProduct->getProducts($ids);

        return new JsonModel(array(
            'products' => $products
        ));
    }

    public function categorizarAction()
    {
        $request = $this->getRequest();
        $ids = $request->getPost('product');
        $category_id = $request->getPost('category_id');
        
        $m = ModelFactory::getInstance()->getProductCategoryMapper();
        $errors = array();
        $m->excluirSelecionados($ids);
        foreach ($ids as $id) :
            try {
                $model = new ProductCategoryModel();
                $model->setCategoryId($category_id);
                $model->setProductId($id);
                $m->saveProductCategoryModel($model);
            } catch (Exception $e) {
                $errors[] = $id;
            }
        endforeach;

        return new JsonModel(array(
            'status' => true,
            'errors' => $errors
        ));
    }

    public function pesquisarAction()
    {
        $request = $this->getRequest();
        $search = $request->getPost('t');

        $mProduct = ModelFactory::getInstance()->getProductMapper(); 
        $products = $mProduct->searchProducts($search);

        $vm = new ViewModel(array(
            'products' => $products
        ));
        $vm->setTerminal(true);
        return $vm;
    }

    public function notfoundAction()
    {
    	return new ViewModel();
    }
}
