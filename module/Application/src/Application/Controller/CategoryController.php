<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Manager\Model\ModelFactory;

class CategoryController extends AbstractActionController
{
    public function __construct() {
    }
    
    public function indexAction()
    {
        $request = $this->getRequest();
        $category_id = $request->getQuery('id');
        
        $mCategory = ModelFactory::getInstance()->getCategoryMapper();
        $mCollection = ModelFactory::getInstance()->getCollectionMapper();

        $category = $mCategory->getCategoryModel($category_id);

        $mProductCategory = ModelFactory::getInstance()->getProductCategoryMapper();
        $products = $mProductCategory->getProdutosPorCategoria($category_id);
        //print_r($products);
        
        return new ViewModel(array(
            'products' => $products,
            'category' => $category->toArray(),
            'categories' => $mCategory->getCategoryModels()->toArray(),
            'collections' => $mCollection->getCollectionModels()->toArray(),
        ));
    }

    public function todasAction()
    {
        $mCollection = ModelFactory::getInstance()->getCollectionMapper();
        $allCollections = $mCollection->getColecoes();
        
        return new ViewModel(array(
            'collections' => $mCollection->getCollectionModels()->toArray(),
            'allCollections' => $allCollections,
        ));
    }

    public function notfoundAction()
    {
    	return new ViewModel();
    }
}
