<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Manager\Model\ModelFactory;

class CollectionController extends AbstractActionController
{
    public function __construct() {
    }
    
    public function indexAction()
    {
        $request = $this->getRequest();
        $collection_id = $request->getQuery('id');
        
        $mCollection = ModelFactory::getInstance()->getCollectionMapper();
        
        $collection = $mCollection->getCollectionModel($collection_id);

        $mProductCollection = ModelFactory::getInstance()->getProductCollectionMapper();
        $products = $mProductCollection->getProdutosPorColecao($collection_id);
        //print_r($products);

        $mCategory   = ModelFactory::getInstance()->getCategoryMapper();
        
        return new ViewModel(array(
            'products' => $products,
            'collection' => $collection->toArray(),
            'collections' => $mCollection->getCollectionModels()->toArray(),
            'categories' => $mCategory->getCategoryModels()->toArray(),
        ));
    }

    public function todasAction()
    {
        $mCollection = ModelFactory::getInstance()->getCollectionMapper();
        $allCollections = $mCollection->getColecoes();
        $mCategory   = ModelFactory::getInstance()->getCategoryMapper();
        
        return new ViewModel(array(
            'collections' => $mCollection->getCollectionModels()->toArray(),
            'allCollections' => $allCollections,
            'categories' => $mCategory->getCategoryModels()->toArray(),
        ));
    }

    public function notfoundAction()
    {
    	return new ViewModel();
    }
}
