<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Model\MySessionManager;

class AuthenticateController extends AbstractActionController
{
    
    protected $_session;
    
    protected $_user;
    
    protected $_urls;
    
    public function __construct() 
    {
        $this->_session = new MySessionManager();
        $this->_urls = array("anunciar");
    }
    
    public function indexAction()
    {
        
        $request = $this->getRequest();
        $redirect = $request->getQuery("u", null);
        
        if ($this->_session->isAuthenticate()) {
            if (!is_null($redirect) && in_array($redirect, $this->_urls)) {
                return $this->redirect()->toUrl('/' . $redirect);
            } else {
                return $this->redirect()->toUrl('/painel/anuncios');
            }
        } else {
            // if (!is_null($redirect) && in_array($redirect, $this->_urls)) {
            //     return $this->redirect()->toUrl('/autenticar?u=' . $redirect);
            // } else {
            //     return $this->redirect()->toUrl('/autenticar');
            // }
        }
        
        return new ViewModel($return);
    }
}
