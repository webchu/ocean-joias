<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Manager\Model\ModelFactory;
use Manager\Model\ProductCategory\ProductCategoryModel;
use Zend\View\Model\JsonModel;

class BudgetController extends AbstractActionController
{
    public function __construct() {
    }

    public function orcarAction()
    {
        $request = $this->getRequest();
        $ids = $request->getPost('product');
        $quantities = $request->getPost('quantity');
        $category_id = $request->getPost('category_id');
        
        $mBudget = ModelFactory::getInstance()->getBudgetMapper();
        $mBudgetItems = ModelFactory::getInstance()->getBudgetItemsMapper();

        $modelBudget = new \Manager\Model\Budget\BudgetModel();
        $modelBudget->setName($request->getPost('name'));
        $modelBudget->setEmail($request->getPost('email'));
        $modelBudget->setPhone($request->getPost('phone'));
        $modelBudget->setMessage($request->getPost('message'));
        $modelBudget->setDate(NULL);
        $idBuget = $mBudget->saveBudgetModel($modelBudget);
        foreach ($ids as $cont => $id) :
            try {
                $modelBudgetItems = new \Manager\Model\BudgetItems\BudgetItemsModel();
                $modelBudgetItems->setBudgetId($idBuget);
                $modelBudgetItems->setProductId($id);
                $modelBudgetItems->setQuantity($quantities[$cont]);
                $mBudgetItems->saveBudgetItemsModel($modelBudgetItems);
            } catch (Exception $e) {
            }
        endforeach;

        return new JsonModel(array(
            'status' => true
        ));
    }

    public function notfoundAction()
    {
    	return new ViewModel();
    }
}
