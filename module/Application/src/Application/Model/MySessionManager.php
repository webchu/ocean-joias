<?php

namespace Application\Model;

use Zend\Session\Config\StandardConfig;
use Zend\Session\SessionManager;
use Zend\Session\Container;

class MySessionManager
{
    private $_manager;
    private $_user = null;
    
    
    public function __construct($name = "customSession")
    {
        $config = new StandardConfig();
        $config->setOptions(array(
            'remember_me_seconds' => 1800,
            'name' => $name,
        ));
        $this->_manager = new SessionManager($config);
        Container::setDefaultManager($this->_manager);
        $this->_user = new Container('user');
    }
    
    public function setUser($user)
    {
        if (!empty($user)) {
            $this->_user->user = $user;
        }
    }
    
    public function clear()
    {
        $container = new Container('user');
        $manager = $container->getManager();
        $manager->destroy(['clear_storage' => true]);
    }
    
    public function getUser()
    {
        return $this->_user->user;
    }
    
    public function isAuthenticate()
    {
        if (isset($this->_user->user) && !empty($this->_user->user))
        {
            return true;
        }
        return false;
    }
    
    public function setProduct($value)
    {
        $this->_user->product = $value;
    }
    
    public function getProduct()
    {
        return $this->_user->product;
    }
}