<?php
return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=bdtest_5;host=bdtest-5.mysql.uhserver.com',
        //'dsn' => 'mysql:dbname=giuseppelopes13;host=localhost',
        'driver_options' => array(
            1002 => 'SET NAMES \'UTF8\'',
        ),
        'username' => 'bdtest_5',
        //'username' => 'root',
        'password' => '1qaz2wsx@',
        /*
        'adapters' => array(
            'api-service' => array(
                'database' => 'hdmotos',
                'driver' => 'PDO_Mysql',
                'hostname' => 'aa7o7l0qseyyz4.cbbuzswioqlz.us-west-2.rds.amazonaws.com',
                //'hostname' => 'localhost',
                //'username' => 'root',
                'username' => 'hdmotos',
                'password' => 'asd987687daSDA',
            ),
        ),
        */
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\\Db\\Adapter\\Adapter' => 'Zend\\Db\\Adapter\\AdapterServiceFactory',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
    ),
);
