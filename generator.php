<?php

use Zend\Config\Reader\Ini;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Metadata\Metadata;
use Zend\Db\Metadata\Object\ColumnObject;
use Zend\Db\Metadata\Object\ConstraintObject;
use Zend\Db\Metadata\Object\TableObject;
use Zend\Mvc\Application;

error_reporting(E_ALL & ~E_NOTICE & ~E_USER_NOTICE);

$_SERVER['REQUEST_URI'] = '/';

require 'init_autoloader.php';

$application = Application::init(require 'config/application.config.php');

$zodeken2 = new Zodeken2($application, __DIR__);
$zodeken2->run();

class Zodeken2
{
    /**
     * @var string
     */
    const DEFAULT_DB_ADAPTER_KEY = 'Zend\Db\Adapter\Adapter';

    /**
     *
     * @var array
     */
    protected $configs = array();

    /**
     *
     * @var Adapter
     */
    protected $dbAdapter;

    /**
     *
     * @var string
     */
    protected $moduleName;

    /**
     *
     * @var string
     */
    protected $workingDir;

    /**
     *
     * @param Application $application
     */
    public function __construct(Application $application, $workingDir)
    {
        $this->workingDir = $workingDir;

        do {
            $dbAdapterServiceKey = $this->prompt(
                'Service key for db adapter [Zend\Db\Adapter\Adapter]: '
            );

            if ('' === $dbAdapterServiceKey) {
                $dbAdapterServiceKey = self::DEFAULT_DB_ADAPTER_KEY;
            }

            if (!$application->getServiceManager()->has($dbAdapterServiceKey)) {
                $isAdapterOk = false;
                echo "Service key $dbAdapterServiceKey does exist", PHP_EOL;
            } else {
                $isAdapterOk = true;
            }

        } while (!$isAdapterOk);

        $this->dbAdapter = $application->getServiceManager()->get(
            $dbAdapterServiceKey
        );

        if (!$this->dbAdapter) {
            throw new Exception("Database is not configured");
        }
    }

    public function run()
    {
        $configFile = $this->workingDir . '/generator.ini';
        $configs = array();

        // read ini configs
        if (is_readable($configFile)) {
            $iniReader = new Ini();
            $configs = $iniReader->fromFile($configFile);
        } else {
            echo "\nNotice: $configFile does not exist\n";
        }

        echo "\n\nWARNING: please backup your existing code!!!\n\n";

        do {
            $moduleName = $this->prompt("Enter module name: ");
        } while ('' === $moduleName);

        $this->moduleName = $moduleName;

        $tableList = isset($configs['tables'])
            && isset($configs['tables'][$moduleName])
            && '' !== $configs['tables'][$moduleName]
            ? preg_split('#\s*,\s*#', $configs['tables'][$moduleName])
            : null;

        if (null === $tableList) {
            $shouldContinue = $this->prompt(
                "Table list of $moduleName module is not set, "
                    + "continue with ALL tables in db? y/n [y]: "
            );

            if ('n' === strtolower($shouldContinue)) {
                echo 'Exiting...', PHP_EOL;
            }
        }

        //echo "Configs = \n". print_r($configs, 1) . "\n";

        //echo "Please wait...\n";

        //echo "Table List = \n". print_r($tableList, 1) . "\n";
        
        $serviceFactoryMethods = array();

        foreach ($this->getTables() as $table) {

            $tableName = $table->getName();

            // check if the table is in the list, can use in_array because
            // performance does not really matter here
            if (null !== $tableList && !in_array($tableName, $tableList)) {
                continue;
            }

            echo $tableName, "\n";

            $this->generateModel($table);
            $this->generateMapper($table);
            $this->generateController($table);
            $this->generateView($table, array("index", "create", "delete"));

            $serviceFactoryMethods[] = $this->getMapperFactoryCode($table);
        }

        $factoryCode = $this->getModelFactoryCode(
            implode('', $serviceFactoryMethods)
        );

        $this->writeFile(
            sprintf(
                '%s/module/%s/src/%s/Model/ModelFactory.php',
                $this->workingDir,
                $this->moduleName,
                $this->moduleName
            ),
            $factoryCode,
            false,
            true
        );
    }
    
    protected function generateController(TableObject $table)
    {
        $controllerName = $this->toCamelCase($table->getName());
        $controllerNameMin = strtolower($controllerName);
        $modelName = $controllerName . "Model";
        $code = <<<TABLE
<?php

namespace $this->moduleName\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Manager\Model\ModelFactory;
use Manager\Model\\$controllerName\\$modelName;

class {$controllerName}Controller extends AbstractActionController
{

    protected \$_{$controllerNameMin} = null;
    
    public function __construct()
    {
        \$this->_{$controllerNameMin} = ModelFactory::getInstance()->get{$controllerName}Mapper();
    }
    
    public function indexAction()
    {
        return new ViewModel(array(
            "elements" => \$this->_{$controllerNameMin}->get{$controllerName}Models(),
            "entity" => "{$controllerNameMin}"
        ));
    }
    
    public function createAction()
    {
        \$request = \$this->getRequest();
        if (\$request->isPost()) {
            \$model = new $modelName();
            \$model->exchangeArray(\$request->getPost());
            \$this->_{$controllerNameMin}->save{$controllerName}Model(\$model);
            return \$this->redirect()->toRoute(strtolower('$this->moduleName'), array("controller" => "$controllerNameMin"));
        } else {
            \$id = (int) \$this->params()->fromRoute('id', 0);
            if (!is_null(\$id) && \$id > 0) {
                \$model = \$this->_{$controllerNameMin}->get{$controllerName}Model(\$id);
                if (!is_object(\$model)) {
                    throw new \\Exception("Not found Model");
                }
            } else {
                \$model = new {$controllerName}Model();
            }
        }
        return new ViewModel(array(
            "model" => \$model
        ));
    }
    
    public function removeAction()
    {
        \$id = (int) \$this->params()->fromRoute('id', 0);
        if (!is_null(\$id) && \$id > 0) {
            \$model = \$this->_{$controllerNameMin}->get{$controllerName}Model(\$id);
            if (is_object(\$model)) {
                \$this->_{$controllerNameMin}->delete{$controllerName}Model(\$model);
            } else throw new \Exception("{$controllerName} Not Found", 1);
        }
        return \$this->redirect()->toRoute(strtolower('$this->moduleName'), array("controller" => "{$controllerNameMin}"));
    }
}
TABLE;
        $filename = sprintf(
            '%s/module/%s/src/%s/Controller/%sController.php',
            $this->workingDir,
            $this->moduleName,
            $this->moduleName,
            $controllerName,
            $controllerName
        );
        
        $this->writeFile($filename, $code, false, true);
    }
    
    protected function generateView(TableObject $table, array $actions)
    {
        $controllerName = $this->toCamelCase($table->getName());
        $controllerNameMin = strtolower($controllerName);
        
        foreach($actions as $action) {
            $code = $this->getViewFromAction($action, $table);;
            $filename = sprintf(
                '%s/module/%s/view/%s/%s/%s.phtml',
                $this->workingDir,
                $this->moduleName,
                strtolower($this->moduleName),
                $controllerNameMin,
                strtolower($action)
            );
            
            $this->writeFile($filename, $code, false, true);
        }
    }
    
    protected function getViewFromAction($action, $table) {
        switch($action) {
            case "index":
                $code = $this->getIndexView($table);
                break;
            case "create":
                $code = $this->getCreateView($table);
                break;
            default:
                $code = "--";
        }
        return $code;
    }
    
    protected function getCreateView($table) {
        $controller = strtolower($table->getName());
        $moduleName = strtolower($this->moduleName);
        $code = <<<VIEW
<?php
\$modelArray = \$this->model->toArray();
\$fields = array_keys(\$modelArray);
?>
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header"><?php echo \$this->translate(ucfirst($controller), __NAMESPACE__); ?> > <?php echo \$this->translate('Add', __NAMESPACE__); ?></h4>
    </div>
</div>
<?php if(isset(\$fields) && count(\$fields) > 0): ?>
    <?php \$haveStatus = false; ?>
    <form action="<?php echo \$this->url(strtolower('$this->moduleName'), array('controller' => '$controller', 'action' => 'create'));?>" 
        method="POST" name="entity" id="entity" class="form-horizontal" role="form">
        <table>
            <?php foreach(\$fields as \$field):?>
                <?php if (!in_array(strtolower(\$field), array("status"))): ?>
                    <?php if (strtolower(\$field) == "id"): ?>
                        <tr class="hide">
                    <?php else: ?>
                        <tr>
                    <?php endif; ?>
                            <td>
                                <label for="<?php echo strtolower(\$field); ?>"><?php echo \$this->translate(ucfirst(\$field), __NAMESPACE__); ?></label>
                            </td>
                            <td>
                                <?php if (in_array(strtolower(\$field), array("text", "description"))): ?>
                                <textarea name="<?php echo \$field; ?>" class="<?php echo strtolower(\$field); ?>"><?php echo \$modelArray[\$field]; ?></textarea>
                                <?php else: ?>
                                <input type="text" <?php if (strpos(\$field, "date") !== false) { echo "data-mask=\"99/99/9999\""; } ?> name="<?php echo \$field; ?>" value="<?php echo \$modelArray[\$field]; ?>" class="<?php echo strtolower(\$field); ?>">
                                <?php endif; ?>
                            </td>
                         </tr>
                <?php else: ?>
                    <?php \$haveStatus = true; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php if (\$haveStatus === true): ?>
            <tr>
                <td><label for="status"><?php echo \$this->translate("Status", __NAMESPACE__); ?></label></td>
                <td>
                    <select name="status" id="status">
                        <?php if(\$this->model->getStatus() == 0 || !empty(\$this->model->getStatus())): ?>
                            <?php if(\$this->model->getStatus() == 1): ?>
                                <option value="1" selected >Ativo</option>
                                <option value="0">Inativo</option>
                            <?php else: ?>
                                <option value="0" selected >Inativo</option>
                                <option value="1" >Ativo</option>
                            <?php endif; ?>
                        <?php else: ?>
                                <option value="0" seleceted>Selecione um Status</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                        <?php endif; ?>
                    </select>
                </td>
            </tr>
            <?php endif; ?>
            
                 <tr>
                     <td></td>
                     <td>
                         <input type="submit" name="submit" id="submitbutton" value="<?php echo \$this->translate(!empty(\$modelArray[\$field]) ? 'Edit' : 'Add', __NAMESPACE__); ?>" class="form-control btn btn-success">
                     </td>
                 </tr>
        </table>
    </form>
<?php endif;?>
<hr>
<script type="text/javascript" src="<?php echo \$this->basePath('js/$moduleName/$controller/create.js'); ?>"></script>
VIEW;
        return $code;
    }
    
    protected function getIndexView($table) {
        $controller = strtolower($table->getName());
        $moduleName = strtolower($this->moduleName);
        $code = <<<VIEW
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header"><?php echo \$this->translate(ucfirst(\$this->entity), __NAMESPACE__); ?></h4>
    </div>
    <div class="col-lg-12">
        <a href="<?php 
            echo \$this->url(strtolower('$this->moduleName'), 
                array('controller' => $controller, 'action' => 'create'));?>" class="btn btn-primary btn-sm active" role="button"><?php echo \$this->translate("Add", __NAMESPACE__); ?></a>
    </div>
</div>
<?php if(isset(\$this->elements) && count(\$this->elements) > 0): ?>
<?php \$headerSetted = false; ?>
<?php \$statusIndex = false; ?>
    <div class="table-responsive">
        <table class="table table-hover">
            <?php foreach(\$this->elements as \$element):?>
                <?php if (!\$headerSetted):  ?>
                <thead>
                    <?php \$headers = array_keys(\$element->toArray()); ?>
                    <tr>
                        <?php foreach(\$headers as \$key => \$header):?>
                            <?php if(strtolower(\$header) == "status") { \$statusIndex = \$key; } ?>
                            <th><?php echo \$this->translate(ucfirst(\$header), __NAMESPACE__); ?></th>
                        <?php endforeach; ?>
                            <th><?php echo \$this->translate("Actions", __NAMESPACE__); ?></th>
                    </tr>
                    <?php \$headerSetted = true; ?>
                </thead>
                <tbody>
                <?php endif; ?>
                <?php \$values = array_values(\$element->toArray()); ?>
                
                <tr data-id="<?php echo \$element->getId(); ?>">
                    <?php foreach(\$values as \$key =>  \$value):?>
                        <td>
                            <?php if(\$statusIndex != false && \$statusIndex == \$key): ?>
                                <?php if (\$value == 1): ?>
                                    <?php echo \$this->translate("Ativo", __NAMESPACE__); ?>
                                <?php else: ?>        
                                    <?php echo \$this->translate("Inativo", __NAMESPACE__); ?>
                                <?php endif; ?>        
                            <?php else: ?>
                            <?php echo \$this->translate(\$value, __NAMESPACE__); ?>
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                        <td>
                            <a href="<?php echo \$this->url(strtolower('$this->moduleName'), 
                                array('controller' => $controller, 'action' => 'create', "id" => \$element->getId())); ?>" class="edit"><?php echo \$this->translate("edit", __NAMESPACE__); ?></a>
                            <a href="<?php echo \$this->url(strtolower('$this->moduleName'), 
                                array('controller' => $controller, 'action' => 'remove', "id" => \$element->getId())); ?>" class="delete"><?php echo \$this->translate("delete", __NAMESPACE__); ?></a>
                        </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
<script type="text/javascript" src="<?php echo \$this->basePath('js/$moduleName/$controller/index.js'); ?>"></script>
VIEW;
        return $code;
    }

    /**
     * Get code of model factory class for each module
     *
     * @param string $factoriesCode
     * @return string
     */
    protected function getModelFactoryCode($factoriesCode)
    {
        return

        $factoryCode = <<<MODULE
<?php

namespace $this->moduleName\Model;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ModelFactory implements ServiceLocatorAwareInterface
{

    /**
     * @var ServiceLocatorInterface
     */
    protected \$serviceLocator;

    /**
     * @var \\$this->moduleName\Model\ModelFactory
     */
    static protected \$instance;

    /**
     * @return \\$this->moduleName\Model\ModelFactory
     */
    static public function getInstance()
    {
        if (!self::\$instance) {
            self::\$instance = new self;
        }
        return self::\$instance;
    }

    private function __construct() {}

    private function __clone() {}
$factoriesCode

    public function setServiceLocator(ServiceLocatorInterface \$serviceLocator)
    {
        \$this->serviceLocator = \$serviceLocator;
    }

    public function getServiceLocator()
    {
        return \$this->serviceLocator;
    }
}
MODULE;
    }

    /**
     *
     * @param TableObject $table
     * @return string
     */
    protected function getMapperFactoryCode(TableObject $table)
    {
        $tableName = $table->getName();

        $modelName = $this->toCamelCase($tableName);

        $getMapperMethod = 'get' . $modelName . 'Mapper';
        $getTableGatewayMethod = 'get' . $modelName . 'TableGateway';

        return <<<CODE

    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    private function $getTableGatewayMethod()
    {
        \$dbAdapter = \$this->serviceLocator->get('Zend\Db\Adapter\Adapter');
        \$resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        \$resultSetPrototype->setArrayObjectPrototype(new \\$this->moduleName\Model\\$modelName\\{$modelName}Model());
        return new \Zend\Db\TableGateway\TableGateway('$tableName', \$dbAdapter, null, \$resultSetPrototype);
    }

    /**
     * @return \\$this->moduleName\Model\\$modelName\\{$modelName}Mapper
     */
    public function $getMapperMethod()
    {
        \$tableGateway = \$this->$getTableGatewayMethod();
        \$mapper = new \\$this->moduleName\Model\\$modelName\\{$modelName}Mapper(\$tableGateway);
        \$mapper->setServiceLocator(\$this->serviceLocator);
        return \$mapper;
    }
CODE;
    }

    /**
     *
     * @param TableObject $table
     */
    protected function generateMapper(TableObject $table)
    {
        $modelName = $this->toCamelCase($table->getName());

        $primaryKey = array();
        $indexes = array();
        $mappingCode = '';
        $indexCode = '';

        foreach ($table->getConstraints() as $constraint)
        {
            /* @var $constraint ConstraintObject */

            $constraintType = $constraint->getType();

            if ('PRIMARY KEY' === $constraintType) {
                $primaryKey = $constraint->getColumns();
            }

            $indexes[] = $constraint->getColumns();
        }
        echo "\nAqui 1 \n" . print_r($indexes, 1) . "\n";
        if (isset($indexes[0])) {
            $indexCodeArray = array();

            foreach ($indexes as $index)
            {
                $singleIndexCode = $this->getMethodsOfIndex($index, $modelName);

                if (!is_string($singleIndexCode)) {
                    continue;
                }

                $indexCodeArray[] = $singleIndexCode;
            }

            $indexCode = implode('', $indexCodeArray);
        }

        if (count($primaryKey) === 1) {
            $mappingCode = $this->getPrimaryKeyCode($primaryKey, $modelName);
        }

        $code = <<<TABLE
<?php

namespace $this->moduleName\Model\\$modelName;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

class {$modelName}Mapper implements ServiceLocatorAwareInterface
{
    /**
     * @var TableGateway
     */
    protected \$tableGateway;

    /**
     * @var ServiceLocatorInterface
     */
    protected \$serviceLocator;

    public function __construct(TableGateway \$tableGateway)
    {
        \$this->tableGateway = \$tableGateway;
    }

    public function get{$modelName}ModelByCriteria(array \$criteria)
    {
        return \$this->tableGateway->select(\$criteria);
    }

$mappingCode
$indexCode

    public function get{$modelName}ModelsAsArray() {
        \$arrayResult = array();
        \$rowset = \$this->get{$modelName}Models();
        foreach(\$rowset as \$row) {
            \$arrayResult[\$row->getId()] = \$row->toArray();
        }
        return \$arrayResult;
    }

    public function setServiceLocator(ServiceLocatorInterface \$serviceLocator)
    {
        \$this->serviceLocator = \$serviceLocator;
    }

    public function getServiceLocator()
    {
        return \$this->serviceLocator;
    }
}
TABLE;
        $filename = sprintf(
            '%s/module/%s/src/%s/Model/%s/%sMapper.php',
            $this->workingDir,
            $this->moduleName,
            $this->moduleName,
            $modelName,
            $modelName
        );

        if (file_exists($filename)) {
            $existingCode = file_get_contents($filename);
            $startPoint = 'protected $serviceLocator;';
            $endPoint = 'public function __construct(TableGateway $tableGateway)';
            $customCode = substr($existingCode, strpos($existingCode, $startPoint) + strlen($startPoint), strpos($existingCode, $endPoint) - strpos($existingCode, $startPoint) - strlen($startPoint));

            $code = preg_replace('#' . preg_quote($startPoint, '#') . '\s*' . preg_quote($endPoint, '#') . '#si', "$startPoint$customCode$endPoint", $code);

            $startPoint = "namespace $this->moduleName\Model\\$modelName;";
            $endPoint = "class {$modelName}Mapper implements ServiceLocatorAwareInterface";

            $customCode = substr($existingCode, strpos($existingCode, $startPoint) + strlen($startPoint), strpos($existingCode, $endPoint) - strpos($existingCode, $startPoint) - strlen($startPoint));

            $code = preg_replace('#' . preg_quote($startPoint, '#') . '.*' . preg_quote($endPoint, '#') . '#si', "$startPoint$customCode$endPoint", $code);
        }

        $this->writeFile($filename, $code, false, true);
    }

    /**
     *
     * @param array $primaryKey
     * @param string $modelName
     * @return string
     */
    protected function getPrimaryKeyCode($primaryKey, $modelName)
    {
        $primaryKeyCamelCase = $this->toCamelCase($primaryKey[0]);

        return <<<CODE

    /**
     *
     * @return resultset
     */
    public function get{$modelName}Models()
    {
        \$tableGateway = \$this->tableGateway;
        \$rowset = \$tableGateway->select(function (Select \$select) {
             \$select->order('id DESC');
        });
        return \$rowset;
    }

    /**
     * @param int \$id
     * @return {$modelName}Model
     */
    public function get{$modelName}Model(\$id)
    {
        return \$this->tableGateway->select(array('$primaryKey[0]' => \$id))->current();
    }

    /**
     * @param {$modelName}Model \$model
     */
    public function save{$modelName}Model({$modelName}Model \$model)
    {
        \$id = \$model->get$primaryKeyCamelCase();

        if (!\$id) {
            \$this->tableGateway->insert(\$model->toArray());
        } else {
            \$this->tableGateway->update(\$model->toArray(), array('$primaryKey[0]' => \$id));
        }
    }

    /**
     *
     * @param {$modelName}Model|int \$model
     */
    public function delete{$modelName}Model(\$model)
    {
        if (\$model instanceof {$modelName}Model) {
            \$id = \$model->get$primaryKeyCamelCase();
        } else {
            \$id = \$model;
        }

        \$this->tableGateway->delete(array('$primaryKey[0]' => \$id));
    }
CODE;
    }

    /**
     *
     * @param type $index
     * @param string $modelName
     * @return string|boolean
     */
    protected function getMethodsOfIndex($index, $modelName)
    {
        echo "\nAqui 2 \n" . print_r($index, 1) . "\n";
        $camelCaseColumns = $index;
        $functionNames = array();

        foreach ($camelCaseColumns as &$camelCaseColumn)
        {
            $camelCaseColumn = $this->toCamelCase($camelCaseColumn);
        }

        $vars = array();

        foreach ($camelCaseColumns as $var)
        {
            $var[0] = strtolower($var[0]);
            $vars[] = $var;
        }

        $functionNameResultSet = "get{$modelName}ModelSetBy" . implode('And', $camelCaseColumns);
        $functionNameResult = "get{$modelName}ModelsBy" . implode('And', $camelCaseColumns);

        if (isset($functionNames[$functionNameResult]) || isset($functionNames[$functionNameResultSet])) {
            return false;
        }

        $functionNames[$functionNameResult] = 1;
        $functionNames[$functionNameResultSet] = 1;

        $argListArray = array();
        $varCommentsArray = array();

        foreach ($vars as $var)
        {
            $argListArray[] = '$' . $var;
            $varCommentsArray[] = "     * @param mixed $$var";
        }
        $argList = implode(', ', $argListArray);
        $varComments = implode("\n", $varCommentsArray);

        $whereArray = array();

        foreach ($index as $offset => $indexColumn)
        {
            $whereArray[] = "'$indexColumn' => $$vars[$offset]";
        }

        $where = implode(",\n            ", $whereArray);

        return <<<CODE


    /**
     *
$varComments
     * @return {$modelName}Model
     */
    public function $functionNameResult($argList)
    {
        return \$this->tableGateway->select(array($where));
    }


    /**
     *
$varComments
     * @return ResultSet
     */
    public function $functionNameResultSet($argList)
    {
        return \$this->tableGateway->select(array($where));
    }
CODE;
    }

    protected function generateModel(TableObject $table)
    {
        $modelName = $this->toCamelCase($table->getName());

        $fieldsCode = array();
        $getterSetters = array();

        foreach ($table->getColumns() as $column)
        {
            /* @var $column ColumnObject */
            $fieldName = $column->getName();
            $fieldNameCamelCase = $varName = $this->toCamelCase($fieldName);
            $varName[0] = strtolower($varName[0]);
            $defaultValue = var_export($column->getColumnDefault(), true);

            $getterSetters[] = "
    public function get$fieldNameCamelCase()
    {
        return \$this->data['$fieldName'];
    }

    public function set$fieldNameCamelCase(\$$varName)
    {
        \$this->data['$fieldName'] = \$$varName;
    }";

            $fieldsCode[] = "
        '$fieldName' => $defaultValue,";
        }

        $fieldsCode = "array(" . implode('', $fieldsCode) . '
    )';
        $getterSettersCode = implode('', $getterSetters);

        $code = <<<MODEL
<?php

namespace $this->moduleName\Model\\$modelName;

class {$modelName}Model
{

    protected \$data = $fieldsCode;
$getterSettersCode

    public function exchangeArray(\$data)
    {
        foreach (\$data as \$key => \$value)
        {
            if (!array_key_exists(\$key, \$this->data)) {
                continue;//throw new \Exception("\$key field does not exist in " . __CLASS__);
            }
            \$this->data[\$key] = \$value;
        }
    }

    public function toArray()
    {
        return \$this->data;
    }
    
    public function getMyName()
    {
        return strtolower("{$modelName}");
    }
}
MODEL;

        $this->writeFile(
            sprintf(
                '%s/module/%s/src/%s/Model/%s/%sModel.php',
                $this->workingDir,
                $this->moduleName,
                $this->moduleName,
                $modelName,
                $modelName
            ),
            $code,
            false,
            true
        );
    }

    /**
     *
     * @return TableObject[]
     */
    protected function getTables()
    {
        $metadata = new Metadata($this->dbAdapter);

        return $metadata->getTables();
    }

    /**
     *
     * @param string $filename
     * @param string $contents
     * @param boolean $generatePatchIfExists
     * @param boolean $overwrite
     */
    protected function writeFile($filename, $contents, $generatePatchIfExists = true, $overwrite = false)
    {
        $dir = dirname($filename);

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        if ($overwrite) {
            file_put_contents($filename, $contents);
        } elseif ("service.config.zk" !== substr($filename, -17) && file_exists($filename)) {
            $zodekenFile = $filename . '.zk';
            file_put_contents($zodekenFile, $contents);

            if ($generatePatchIfExists && 'Linux' === PHP_OS) {
                `diff -u $filename $zodekenFile > $filename.patch`;
            }
        } else {
            file_put_contents($filename, $contents);
        }
    }

    /**
     *
     * @param string $name
     * @return string
     */
    protected function toCamelCase($name)
    {
        return implode('', array_map('ucfirst', explode('_', $name)));
    }

    /**
     *
     * @param string $message
     * @return string
     */
    protected function prompt($message)
    {
        echo $message;
        return trim(fgets(STDIN));
    }

}

