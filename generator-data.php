<?php
/**
 * Alguns padroes a seguir:
 *  - Se o type for = text vai virar um textarea
 *  - Se o type for = int(11) E tem relationship, ele monta automatico o select da FK
 */
return array("Manager" => array(

    "product" => array(
        "name" => array(
            "type" => "varchar(150)",
            "default" => "",
            "null" => false,
        ),
        "description" => array(
            "type" => "text"
        ),
        "photo" => array(
            "type" => "varchar(150)",
            "default" => "",
            "null" => false,
        )
    ),
    "category" => array(
        "name" => array(
            "type" => "varchar(150)",
            "default" => "",
            "null" => false,
        ),
        "url" => array(
            "type" => "varchar(150)",
            "default" => "",
            "null" => false,
        ),
        "photo" => array(
            "type" => "varchar(150)",
            "default" => "",
            "null" => false,
        )
    ),
    "collection" => array(
        "name" => array(
            "type" => "varchar(150)",
            "default" => "",
            "null" => false,
        ),
        "url" => array(
            "type" => "varchar(150)",
            "default" => "",
            "null" => false,
        ),
        "photo" => array(
            "type" => "varchar(150)",
            "default" => "",
            "null" => false,
        )
    ),
    "product_category" => array(
        "cateogory_id" => array(
            "type" => "int(11)"
        ),
        "product_id" => array(
            "type" => "int(11)"
        ),
    ),
    "user" => array(
        "name" => array(
            "type" => "varchar(150)",
            "default" => "",
            "null" => false,
        ),
        "email" => array(
            "type" => "varchar(128)",
            "default" => ""
        ),
        "password" => array(
            "type" => "varchar(128)",
            "default" => ""
        ),
        "status" => array(
            "type" => "int(1)",
            "index" => true,
            "null" => false,
            "default" => 1
        ),
        "date_create" => array(
            "type" => "timestamp"
        ),
    ),
));